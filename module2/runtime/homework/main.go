package main

import (
	"fmt"
	"runtime"
)

func main() {
	runtime.Gosched()
	fmt.Println("i can manage")
	go func() {
		fmt.Println("goroutines in Golang!")
	}()
	fmt.Println("and its awesome!")
}

package main

import (
	"fmt"
	"testing"
)

type calc struct {
	a, b   float64
	result float64
}

func NewCalc() *calc { // конструктор калькулятора
	return &calc{}
}

func (c *calc) SetA(a float64) *calc {
	c.a = a
	return c
}

func (c *calc) SetB(b float64) *calc {
	c.b = b

	return c
}

func (c *calc) Do(operation func(a, b float64) float64) *calc {
	c.result = operation(c.a, c.b)

	return c
}

func (c *calc) Result() float64 {
	return (*c).result
}

func multiply(a, b float64) float64 { // реализуйте по примеру divide, sum, average
	return a * b
}

func divide(a, b float64) float64 {
	return a / b
}

func sum(a, b float64) float64 {
	return a + b
}

func average(a, b float64) float64 {
	return (a + b) / 2
}
func main() {
	calc := NewCalc()
	res := calc.SetA(10).SetB(34).Do(multiply).Result()
	fmt.Println(res)
	res2 := calc.Result()
	fmt.Println(res2)

	res = calc.SetA(10).SetB(34).Do(divide).Result()
	fmt.Println(res)
	res2 = calc.Result()
	fmt.Println(res2)
	res = calc.SetA(10).SetB(34).Do(sum).Result()
	fmt.Println(res)
	res2 = calc.Result()
	fmt.Println(res2)
	res = calc.SetA(10).SetB(34).Do(average).Result()
	fmt.Println(res)
	res2 = calc.Result()
	fmt.Println(res2)
	if res != res2 {
		panic("object statement is not persist")
	}
}

func Test_Multilply(t *testing.T) {
	c := NewCalc()

	type testcase struct {
		a, b       float64
		conclusion float64
	}

	test := []testcase{
		{a: 5.0, b: 5, conclusion: 25},
		{a: 3.0, b: 8, conclusion: 24},
		{a: 6.0, b: 4, conclusion: 24},
		{a: 1.0, b: 1, conclusion: 1},
		{a: 0.0, b: 1, conclusion: 0},
		{a: -4.0, b: 1, conclusion: -4},
		{a: 9.0, b: 3, conclusion: 27},
	}

	for _, tt := range test {
		var result = c.SetA(tt.a).SetB(tt.b).Do(multiply).Result()
		if result != tt.conclusion {
			t.Errorf("Conclusion %.2f, не равен  %.2f", tt.conclusion, result)
		}
	}
}

func Test_Divide(t *testing.T) {
	c := NewCalc()

	type testcase struct {
		a, b       float64
		conclusion float64
	}

	test := []testcase{
		{a: 15.0, b: 5, conclusion: 3},
		{a: 24.0, b: 8, conclusion: 3},
		{a: 16.0, b: 4, conclusion: 4},
		{a: 12.0, b: 1, conclusion: 12},
		{a: 0.0, b: 1, conclusion: 0},
		{a: -4.0, b: 4, conclusion: -1},
		{a: 9.0, b: 3, conclusion: 3},
	}

	for _, tt := range test {
		var result = c.SetA(tt.a).SetB(tt.b).Do(divide).Result()
		if result != tt.conclusion {
			t.Errorf("Conclusion %.2f, не равен  %.2f", tt.conclusion, result)
		}
	}
}

func Test_Sum(t *testing.T) {
	c := NewCalc()
	type testcase struct {
		a, b       float64
		conclusion float64
	}

	test := []testcase{
		{a: 3.0, b: 5, conclusion: 8},
		{a: 2.0, b: 8, conclusion: 10},
		{a: 6.0, b: 4, conclusion: 10},
		{a: 1.0, b: 1, conclusion: 2},
		{a: 0.0, b: 1, conclusion: 1},
		{a: -1.0, b: 4, conclusion: 3},
		{a: 9.0, b: 3, conclusion: 12},
	}

	for _, tt := range test {
		var result = c.SetA(tt.a).SetB(tt.b).Do(sum).Result()
		if result != tt.conclusion {
			t.Errorf("Conclusion %.2f, не равен %.2f", tt.conclusion, result)
		}
	}
}

func Test_Average(t *testing.T) {
	c := NewCalc()

	type testcase struct {
		a, b       float64
		conclusion float64
	}

	test := []testcase{
		{a: 4.0, b: 6, conclusion: 5},
		{a: 3.0, b: 13, conclusion: 8},
		{a: 21.0, b: 5, conclusion: 13},
		{a: 1.0, b: 1, conclusion: 1},
		{a: 3.0, b: 1, conclusion: 2},
		{a: 12, b: 10, conclusion: 11},
		{a: 9.0, b: 3, conclusion: 6},
	}

	for _, tt := range test {
		var result = c.SetA(tt.a).SetB(tt.b).Do(average).Result()
		if result != tt.conclusion {
			t.Errorf("Conclusion %.2f, не равен %.2f", tt.conclusion, result)
		}
	}
}

package main

import (
	"fmt"
	"gitlab.com/egory4eff-x/go-kata/module2/packages/greet"
)

func main() {
	greet.Hello()
	fmt.Println(greet.Shark)

	oct := greet.Octopus{
		Name:  "Jesse",
		Color: "red",
	}
	fmt.Println(oct.String())
	oct.Reset()
	fmt.Println(oct.String())
}

package main

import (
	"fmt"
	"unsafe"
)

func main() {
	typeInt()
}

func typeInt() {
	fmt.Println("=== START  type uint ===")
	var uintNumber uint8 = 1 << 7 // делаем сдвиг на середину в uint8
	var min = int8(uintNumber)    // преобразовываем в int8
	uintNumber--                  // минусуем одно значение
	var max = int8(uintNumber)    // преобразовываем в int8
	fmt.Println(min, max)
	fmt.Println("int8 min value:", min, "int8 max value:", max, "size", unsafe.Sizeof(min), "bytes")

	var uintNumber16 uint16 = 1 << 15
	var min16 = int16(uintNumber16)
	uintNumber16--
	var max16 = int16(uintNumber16)
	fmt.Println(min16, max16)
	fmt.Println("int16 min value:", min16, "int16 max value:", max16, "size", unsafe.Sizeof(min16), "bytes")

	var uintNumber32 uint32 = 1 << 31
	var min32 = int32(uintNumber32)
	uintNumber32--
	var max32 = int32(uintNumber32)
	fmt.Println(min32, max32)
	fmt.Println("int32 min value:", min32, "int32 max value:", max32, "size", unsafe.Sizeof(min32), "bytes")

	var uintNumber64 uint64 = 1 << 63
	var min64 = int64(uintNumber64)
	uintNumber64--
	var max64 = int64(uintNumber64)
	fmt.Println(min64, max64)
	fmt.Println("int64 min value:", min64, "int64 max value:", max64, "size", unsafe.Sizeof(min64), "bytes")
	fmt.Println("=== END type uint ===")
}

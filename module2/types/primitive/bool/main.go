package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("size is:", unsafe.Sizeof(n), "bytes")
	typeBool()
}

func typeBool() {
	var b bool
	fmt.Println("Размер в байтах:", unsafe.Sizeof(b)) // Размер в байтах: 1
	var u uint8 = 1                                   // берем uint8 - 8 bit, выставляем значение 1
	fmt.Println(b)                                    // по умолчанию bool имеет значение false
	b = *(*bool)(unsafe.Pointer(&u))                  //берем значение из uint8 проставим в тип bool
	fmt.Println(b)                                    // распечатаем значение true
}

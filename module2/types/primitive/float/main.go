package main

import (
	"fmt"
	"unsafe"
)

func main() {
	typeFloat()
}
func typeFloat() {
	fmt.Println("=== START type float ===")
	//var floatNumber float32
	var uintNumber uint32 = 1 << 29 //осуществляем сдиг на 29 ячеек в 30 позицию
	uintNumber += 1 << 28
	uintNumber += 1 << 27
	uintNumber += 1 << 26
	uintNumber += 1 << 25
	uintNumber += 1 << 21
	var floatNumber = *(*float32)(unsafe.Pointer(&uintNumber))
	fmt.Println(floatNumber)
	fmt.Println("=== END type float ===")
}

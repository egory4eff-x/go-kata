package main

import "fmt"

func main() {
	var numberInt int = 3                        //создаем переменную(объект) типа int , присваиваем значение 3
	var numberFloat float32 = float32(numberInt) //создаем переменную(объект) типа float32,
	// присваиваем numberFloat значение которого является результатом конвертации с типа int в float32
	fmt.Printf("тип: %T,значение: %v", numberFloat, numberFloat)
}

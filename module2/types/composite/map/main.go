package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "Map",
			Stars: 3453,
		},
		{
			Name:  "Slice",
			Stars: 54623,
		},
		{
			Name:  "string",
			Stars: 12356,
		},
		{
			Name:  "int",
			Stars: 85329,
		},
		{
			Name:  "func",
			Stars: 97462,
		},
		{
			Name:  "array",
			Stars: 8392,
		},
		{
			Name:  "Type",
			Stars: 34182,
		},
		{
			Name:  "bool",
			Stars: 67298,
		},
		{
			Name:  "float",
			Stars: 24520,
		},
		{
			Name:  "uint",
			Stars: 62436,
		},
		{
			Name:  "cont",
			Stars: 49382,
		},
		{
			Name:  "interface",
			Stars: 93348,
		},
		{
			Name:  "go",
			Stars: 45321,
		},
	}

	// в цикле запишите в map
	m := make(map[string]int)
	for _, z := range projects {
		m[z.Name] = z.Stars
		fmt.Println(m)
	}

	// в цикле пройдитесь по мапе и выведите значения в консоль
	for k, v := range m {
		fmt.Println(k, v)
	}
}

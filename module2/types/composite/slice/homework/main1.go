package main

import "fmt"

func main() {
	s := []int{1, 2, 3}      //первый слайс
	s2 := []int{1, 2, 3}     //второй слайс
	Append(&s)               // 1 решение
	fmt.Println(s)           //основной слайс, при выводе видно, что добавляется занчение 4 и увеличивается capacity
	fmt.Println(Append2(s2)) // 2 решение

}

// фунция реализована при помощи указателя на слайс
func Append(s *[]int) {
	*s = append(*s, 4)
	fmt.Println(*s)
}

// функция реализована при помощи передачи приема и возврата слайса
func Append2(s []int) []int {
	s = append(s, 4)
	return s
}

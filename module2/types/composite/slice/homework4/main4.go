package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Eva",
			Age:  13,
		},
		{
			Name: "Victor",
			Age:  28,
		},
		{
			Name: "Dex",
			Age:  34,
		},
		{
			Name: "Billy",
			Age:  21,
		},
		{
			Name: "Foster",
			Age:  29,
		},
	}

	subUsers := users[2:]
	editSecondSlice(subUsers)
	fmt.Println(users[:0:0])
	fmt.Println(users)
}

func editSecondSlice(users []User) []User {
	newSlice := append(users[:0:0], users...)
	for _, n := range users {
		n.Name = "unknown"
	}
	return newSlice
}

package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}
	fmt.Println(users)         //основной слайс
	fmt.Println(yong40(users)) //слайс, кому меньше 40
}

// функция записывает в новый слайс, тех кому меньше 40
func yong40(u []User) []User {
	year := make([]User, 0)
	for _, n := range u {
		if n.Age < 40 {
			year = append(year, n)
		}
	}
	return year
}

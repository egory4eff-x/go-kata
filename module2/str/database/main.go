package main

import (
	"database/sql"
	"fmt"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	database, _ := sql.Open("sqlite3", "./gopher.db")

	defer database.Close()

	statement, _ := database.Prepare("CREATE TABLE IF NOT EXISTS people (id INTEGER PRIMARY KEY AUTOINCREMENT, firstname TEXT, lastname TEXT)")
	_, err := statement.Exec()
	if err != nil {
		os.Exit(1)
	}
	statement, _ = database.Prepare("INSERT INTO people (firstname, lastname) VALUES (?, ?)")
	_, err = statement.Exec("Second", "Ipsum")
	if err != nil {
		os.Exit(1)
	}

	rows, _ := database.Query("SELECT id, firstname FROM people")
	var id int
	var firstname string
	//var lastname string
	for rows.Next() {
		err := rows.Scan(&id, &firstname)
		if err != nil {
			os.Exit(1)
		}
		fmt.Printf("%d: %s \n", id, firstname)
	}
}

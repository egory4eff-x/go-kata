// You can edit this code!
// Click here and start typing.
package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}
	// здесь расположите буфер
	var buffer bytes.Buffer

	// запишите данные в буфер
	for _, v := range data {
		buffer.WriteString(v + "\n")
	}

	// создайте файл
	file, err := os.Create("example.txt")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	_, err = io.Copy(file, &buffer)
	if err != nil {
		panic(err)
	}

	// запишите данные в файл
	newBuffer := bytes.Buffer{}
	newFile, err := os.Open("example.txt")
	if err != nil {
		panic(err)
	}
	defer newFile.Close()

	_, err = io.Copy(&newBuffer, newFile)
	if err != nil {
		panic(err)
	}

	// прочтите данные в новый буфер
	fmt.Println(newBuffer.String())
	// у вас все получится!
}

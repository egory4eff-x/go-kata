package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"os"
)

type Config struct {
	AppName    string `json:"name"`
	Production bool   `json:"prod"`
}

func main() {
	flagConfig := flag.Bool("config.json", false, "output Config") //
	flag.Parse()
	if *flagConfig {

		conf := Config{
			AppName:    "uber",
			Production: true,
		}

		file, err := os.Open("config.json") //открываем файл
		if err != nil {
			fmt.Println(err)
		}
		defer file.Close() //закрытие после прочтения

		byteValue, _ := io.ReadAll(file) //читаем файл
		isValid := json.Valid(byteValue) //проверяем валидный документ или нет
		fmt.Println(isValid)
		json.Unmarshal(byteValue, &conf) //производим декодирование
		fmt.Printf("%#v\n", conf)        //вывод
	}
}

package main

import "fmt"

type Person struct {
	name  string
	age   int
	money float32
}

func main() {
	p := Person{name: "Bob", age: 18, money: 205.00}
	fmt.Println(generateSelfStory(&p))
}

func generateSelfStory(p *Person) string {
	return fmt.Sprintf("Hello, my name is %s. I'm %d y.o. And I also $%.2f  have in my wallet right now.", p.name, p.age, p.money)
}

package main

import (
	"fmt"
	"strings"
	"unicode/utf8"
)

var data = ` Сообщения с телеграм канала Go-go!
я, извините, решил, что вы хоть что-то про регулярки знаете 🙂
С Почтением, с уважением 🙂.

Сообщения с телеграм канала Go-get job:
#Вакансия #Vacancy #Удаленно #Remote #Golang #Backend  
👨‍💻  Должность: Golang Tech Lead
🏢  Компания: PointPay
💵  Вилка: от 9000 до 15000 $ до вычета налогов
🌏  Локация: Великобритания
💼  Формат работы: удаленный
💼  Занятость: полная занятость 

Наша компания развивает 15 продуктов в сфере блокчейн и криптовалюты, функционирующих в рамках единой экосистемы. Мы - одни из лучших в своей нише, и за без малого три года мы создали криптовалютную платформу, которая предоставляет комплексные решения и позволяет пользователям проводить практически любые финансовые операции.
Наш проект победил в номинации "лучший блокчейн-стартап 2019 года" на одной из самых крупных конференций по блокчейн, криптовалютам и майнингу – Blockchain Life в Москве.

У нас очень амбициозные планы по развитию наших продуктов, и на данный момент мы находимся в поиске Golang Technical Lead, чтобы вместе создать революционный и самый технологичный продукт в мире.


  
Мы ожидаем от тебя:  
  
✅ Опыт управления финансовыми IT продуктами;
✅ Опыт постановки задач технической команде и приема результатов;
✅ Понимание и использование современных подходов к разработке (Agile);
✅ Опыт разработки не менее 5 лет;
✅ Отличное владение Go, будет плюсом опыт работы на PHP;
✅ Опыт работы с REST / GRPС (protobuf);
✅ Опыт работы с Laravel;
✅ Отличные навыки SQL (мы используем PostgreSQL);
✅ Опыт работы с Redis или аналогичной системой кеширования;
✅ Опыт работы с Kubernetes, с брокерами сообщений (RabbitMQ, Kafka и др.);
✅ Базовые знания Amazon Web Services.

Будем плюсом:

✅ Опыт разработки криптовалютных кошельков / криптовалютных бирж / криптовалютных торговых роботов / криптовалютных платежных систем или опыт работы в FinTech или банковских продуктах.

Чем предстоит заниматься:  
  
📌 Управлением и взаимодействием с командой;
📌 Постановкой задач команде разработчиков;
📌 Разработкой решений для запуска и сопровождения разрабатываемого компанией ПО;
📌 Выстраиванием эффективного процесса разработки;
📌 Достигать результатов, поддерживать и развивать текущие проекты, развивать новые проекты и продукты, искать и предлагать нестандартные решения задач.

Мы гарантируем будущему коллеге:  
  
🔥 Высокую заработную плату до 15 000$ (в валюте);
🔥 Индексацию заработной платы;
🔥 Полностью удаленный формат работы и максимально гибкий график - работайте из любой точки мира в удобное вам время;
🔥 Оформление по бессрочному международному трудовому договору с UK;
🔥 Отсутствие бюрократии и кучи бессмысленных звонков;
🔥 Корпоративную технику за счет компании;
🔥 Сотрудничество с командой высококвалифицированных специалистов, настоящих профессионалов своего дела;
🔥 Работу над масштабным, интересным и перспективным проектом.


Если в этом описании вы узнали  себя - пишите. Обсудим проект, задачи и все детали :)  
  
Павел,  
✍️TG: @pavel_hr  
📫 papolushkin.wanted@gmail.com`

func main() {
	fmt.Println(len(data))
	fmt.Println(utf8.RuneCountInString(data)) // кол-во символов в тексте

	data = strings.ReplaceAll(data, "🙂", "=)")
	fmt.Println(data)

	eng_data := `Messages from the Go-go telegram channel!
I'm sorry, I decided that you at least know something about the regulars 🙂
With respect, with respect..

Messages from the Go-get job telegram channel:
#Vacancy #Vacancy #Remotely #Remote #Golang #Backend  
👨‍💻  Position: Golang Tech Lead
🏢 Company: PointPay
💵 Fork: from $9,000 to $15,000 before taxes
🌏 Location: United Kingdom
💼 Work format: remote
💼 Employment: full-time 

Our company develops 15 products in the field of blockchain and cryptocurrencies, functioning within a single ecosystem. We are one of the best in our niche, and for almost three years we have created a cryptocurrency platform that provides comprehensive solutions and allows users to conduct almost any financial transactions.
Our project won in the nomination "the best blockchain startup of 2019" at one of the largest conferences on blockchain, cryptocurrencies and mining - Blockchain Life in Moscow.

We have very ambitious plans for the development of our products, and at the moment we are in search of Golang Technical Lead to create together a revolutionary and the most technologically advanced product in the world.


  
We expect from you:  
  
✅ Experience in managing financial IT products;
✅ Experience in setting tasks for the technical team and receiving results;
 Understanding and using modern approaches to development (Agile);
✅ At least 5 years of development experience;
✅ Excellent command of Go, experience in PHP will be a plus;
✅ Experience with REST/ GRPS (protobuf);
✅ Experience with Laravel;
 Excellent SQL skills (we use PostgreSQL);
✅ Experience with Redis or a similar caching system;
✅ Experience working with Kubernetes, with message brokers (RabbitMQ, Kafka, etc.);
✅ Basic knowledge of Amazon Web Services.

Let's be a plus:

✅ Experience in developing cryptocurrency wallets / cryptocurrency exchanges / cryptocurrency trading robots / cryptocurrency payment systems or experience in FinTech or banking products.

What you have to do:  
  
📌 Management and interaction with the team;
📌 Setting tasks for the development team;
📌 Development of solutions for the launch and maintenance of software developed by the company;
📌 Building an effective development process;
Achieve results, support and develop current projects, develop new projects and products, search for and offer non-standard solutions to problems.

We guarantee a future colleague:  
  
🔥 High salary up to $15,000 (in foreign currency);
🔥 Salary indexation;
🔥 Completely remote work format and the most flexible schedule - work from anywhere in the world at your convenience;
🔥 Registration under an indefinite international employment contract with UK;
🔥 Lack of bureaucracy and a bunch of meaningless calls;
🔥 Corporate equipment at the expense of the company;
🔥 Cooperation with a team of highly qualified specialists, real professionals in their field;
🔥 Work on a large-scale, interesting and promising project.


If you recognize yourself in this description, write. Let's discuss the project, tasks and all the details :)  
  
Pavel,  
✍️TG: @pavel_hr  
📫 papolushkin.wanted@gmail.com`
	fmt.Println(utf8.RuneCountInString(eng_data)) //кол-во символов в тексте eng

	LatinByteCount := len(eng_data)
	CyrillicByteCount := len(data)
	res := LatinByteCount / CyrillicByteCount
	fmt.Println(res) //компрессия текста

}

//Выведи количество символов в тексте, текст будет доступен по ссылке.
//
//Замени все смайлы на =), выведи количество символов в строке.
//
//Переведи текст в транслит, выведи количество символов в строке.
//
//Укажи полученную компрессию текста через подсчет байтов в тексте на кириллице и латинице.
//	(res := LatinByteCount/CyrillicByteCount) выведи результат, используя fmt.Println.

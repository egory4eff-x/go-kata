package main

import (
	"fmt"
	"os"
	"strings"
)

var russianTransliteration = map[rune]string{
	'а': "a", 'б': "b", 'в': "v", 'г': "g", 'д': "d", 'е': "e", 'ё': "yo", 'ж': "zh",
	'з': "z", 'и': "i", 'й': "y", 'к': "k", 'л': "l", 'м': "m", 'н': "n", 'о': "o",
	'п': "p", 'р': "r", 'с': "s", 'т': "t", 'у': "u", 'ф': "f", 'х': "kh", 'ц': "ts",
	'ч': "ch", 'ш': "sh", 'щ': "shch", 'ъ': "", 'ы': "y", 'ь': "", 'э': "e", 'ю': "yu",
	'я': "ya",
}

func main() {
	// Создаем текстовый файл
	file, err := os.Create("./example.txt")
	if err != nil {
		fmt.Println("Ошибка:", err)
		return
	}
	defer file.Close()

	data := "домашняя работа"

	// Записываем в файл
	file.WriteString(data)

	//Читаем файл example.txt
	data_file, _ := os.ReadFile("example.txt")
	fmt.Println(string(data_file))

	// Конвертация русских букв в латинские
	output := strings.Builder{}
	for _, c := range string(data_file) {
		if tr, ok := russianTransliteration[c]; ok {
			output.WriteString(tr)
		} else {
			output.WriteRune(c)
		}
	}

	// Записываем в новый файл
	err = os.WriteFile("./example.processed.txt", []byte(output.String()), 0644)
	if err != nil {
		fmt.Println("Ошибка:", err)
		return
	}

	fmt.Println("Успех")
}

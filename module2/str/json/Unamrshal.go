//package main
//
//import (
//	"encoding/json"
//	"fmt"
//)
//
//type Profile struct {
//	Username  string
//	Followers int
//}
//type Student struct {
//	FirstName, LastName string
//	HeightInMerers      float64
//	IsMale              bool
//	Languages           [2]string
//	Subjects            []string
//	Grades              map[string]string
//	Profile             *Profile
//}
//
//func main() {
//	data := []byte(`
//	{
//			"FirstName": "Jo",
//			"HeightInMeters": 1.23,
//			"IsMale": null,
//			"Languages": ["eng","span", "german"],
//			"Subjects": ["math", "science"],
//			"Grades": null,
//			"Profile": {"Followers": 1234}
//	}`)
//
//	var jo Student = Student{
//		IsMale:    true,
//		Languages: [2]string{"Korean", "Chinese"},
//		Subjects:  nil,
//		Grades:    map[string]string{"science": "A+"},
//		Profile:   &Profile{Username: "fofo"},
//	}
//	fmt.Printf("Error: %v\n\n", json.Unmarshal(data, &jo))
//
//	// print `john` struct
//	fmt.Printf("%#v\n\n", jo)
//	fmt.Printf("%#v\n", jo.Profile)
//}
//
//package main
//
//import (
//	"encoding/json"
//	"fmt"
//	"strings"
//)
//
//// Profile declares `Profile` structure
//type Profile struct {
//	Username  string
//	Followers int
//}
//
//// Account declares `Account` structure
//type Account struct {
//	IsMale bool
//	Email  string
//}
//
//// Student declares `Student` structure
//type Student struct {
//	FirstName, lastName string
//	HeightInMeters      float64
//	IsMale              bool
//	Profile
//	Account
//}
//
//func main() {
//
//	// some JSON data
//	data := []byte(`
//	{
//		"FirstName": "John",
//		"HeightInMeters": 1.75,
//		"IsMale": true,
//		"Username": "johndoe91",
//		"Followers": 1975,
//		"Account": { "IsMale": true, "Email": "john@doe.com" }
//	}`)
//
//	// create a data container
//	var john Student
//
//	// unmarshal `data`
//	fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))
//
//	// print `john` struct
//	fmt.Printf("%#v\n", john)
//}

package main

import (
	"encoding/json"
	"fmt"
	"strings"
)

// Profile declares `Profile` structure
type Profile struct {
	Username  string
	Followers string
}

// UnmarshalJSON - implement Unmarshaler interface
func (p *Profile) UnmarshalJSON(data []byte) error {

	// unmarshal JSON
	var container map[string]interface{}
	_ = json.Unmarshal(data, &container)
	fmt.Printf("container: %T / %#v\n\n", container, container)

	// extract interface values
	iuserName, _ := container["Username"]
	ifollowers, _ := container["f_count"]
	fmt.Printf("iuserName: %T/%#v\n", iuserName, iuserName)
	fmt.Printf("ifollowers: %T/%#v\n\n", ifollowers, ifollowers)

	// extract concrete values
	userName, _ := iuserName.(string)    // get `string` value
	followers, _ := ifollowers.(float64) // get `float64` value
	fmt.Printf("userName: %T/%#v\n", userName, userName)
	fmt.Printf("followers: %T/%#v\n\n", followers, followers)

	// assign values
	p.Username = strings.ToUpper(userName)
	p.Followers = fmt.Sprintf("%.2fk", followers/1000)

	return nil
}

// Student declares `Student` structure
type Student struct {
	FirstName string
	Profile   Profile
}

func main() {

	// some JSON data
	data := []byte(`
	{
		"FirstName": "John",
		"Profile": {
			"Username": "johndoe91",
			"f_count": 1975
		}
	}`)

	// create a data container
	var john Student

	// unmarshal `data`
	fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))

	// print `john` struct
	fmt.Printf("%#v\n", john)
}

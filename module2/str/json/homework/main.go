package main

import (
	"encoding/json"
	"fmt"
)

type Profile struct {
	Username  string `json:"uname"`
	Followers int    `json:"followers,omitempty,string"`
}

type Student struct {
	FirstName string  `json:"fname"`
	LastName  string  `json:"lastName"`
	Email     string  `json:"-"`
	Age       int     `json:"-,"`
	IsMale    bool    `json:",string"`
	Profile   Profile `json:""`
}

func main() {
	jo := &Student{
		FirstName: "jo",
		LastName:  "",
		Age:       31,
		Email:     "jojo@mail.com",
		Profile: Profile{
			Username:  "jooj",
			Followers: 124,
		},
	}

	joJSON, _ := json.MarshalIndent(jo, "", " ")
	fmt.Println(string(joJSON))
}

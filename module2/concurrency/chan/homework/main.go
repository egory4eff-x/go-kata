package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const timeout = 30 * time.Second

func joinChannels(chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	go func() {
		// Используем WaitGroup для ожидания завершения всех горутин, которые будут отправлять данные в объединенный канал
		wg := &sync.WaitGroup{}
		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()

				// Отправляем все значения из канала ch в mergedCh
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		// Закрываем mergedCh, чтобы мы могли использовать range для чтения данных из mergedCh
		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func generateData() <-chan int {
	out := make(chan int, 1000)

	// Генерируем случайные целочисленные значения из диапазона 0..99 и отправляем их в канал out
	go func() {
		defer close(out)
		for {
			select {
			case out <- rand.Intn(100):
			}
		}
	}()

	return out
}

func main() {
	ticker := time.NewTicker(timeout)

	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	go func() {
		for {
			select {
			case i := <-a:
				fmt.Println("a", i)
			case i := <-b:
				fmt.Println("b", i)
			case i := <-c:
				fmt.Println("c", i)
			}
		}
	}()

	out := generateData()

	// Отправляем значения из out в каналы a, b, c
	go func() {
		for num := range out {
			a <- num
			b <- num
			c <- num
		}
		// Закрываем каналы a, b, c, чтобы notify горутины-читателя о завершении передачи данных
		close(a)
		close(b)
		close(c)
	}()

	mainChan := joinChannels(a, b, c)

	for num := range mainChan {
		fmt.Println(num)
	}

	time.Sleep(3 * time.Second)
	ticker.Stop()
	fmt.Println("Ticker stopped")
}

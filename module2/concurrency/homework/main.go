package main

import (
	"fmt"
	"time"
)

func main() {
	msg := make(chan string)
	msg2 := make(chan string)

	go func() {
		for {
			time.Sleep(time.Millisecond * 500)
			msg <- "Прошло пол секунды"
		}
	}()

	go func() {
		for {
			time.Sleep(time.Second * 2)
			msg2 <- "Прошло 2 секунды"
		}
	}()

	for {
		select {
		case msg3 := <-msg:
			fmt.Println(msg3)
		case msg3 := <-msg2:
			fmt.Println(msg3)
		}
	}
}

//func main() {
//	urls := []string{
//		"http://google.com/",
//		"http://youtube.com/",
//		"http://github.com/",
//		"http://instagram.com/",
//		"http://medium.com",
//	}
//
//	var wg sync.WaitGroup
//
//	for _, url := range urls {
//		wg.Add(1)
//		go doHTTP(url)
//
//		go func(url string) {
//			doHTTP(url)
//			wg.Done()
//		}(url)
//	}
//
//	wg.Wait()
//}
//
//func doHTTP(url string) {
//	t := time.Now()
//	resp, err := http.Get(url)
//	if err != nil {
//		fmt.Printf("Failed to get <%s>: %s\n", url, err.Error())
//	}
//
//	defer resp.Body.Close()
//
//	fmt.Printf("<%s> - Status Code [%d] - Latency %d ms\n",
//		url, resp.StatusCode, time.Since(t).Milliseconds())
//
//}

//func main() {
//	msg := make(chan string, 2) //буферезированный канал, емкостью 2
//	msg <- "hello"
//	msg <- "world"
//
//	fmt.Println(<-msg)
//	msg <- "!"
//
//	fmt.Println(<-msg)
//	fmt.Println(<-msg)
//}

//func main() {
//	message := make(chan string)
//
//	go func() {
//		for i := 1; i <= 10; i++ {
//			message <- fmt.Sprintf("%d", i)
//			time.Sleep(time.Millisecond * 500)
//		}
//
//		close(message)
//	}()
//
//	for msg := range message {
//		fmt.Println(msg)
//	}
//}

//func main() {
//	message := make(chan string)
//
//	go func() {
//		time.Sleep(2 * time.Second)
//		message <- "hello"
//	}()
//	fmt.Println(<-message)
//}

//
//func main() {
//	t := time.Now()
//	rand.Seed(t.UnixNano())
//
//	go parseURL("http:/example.com/")
//	parseURL("http:/youtube.com/")
//
//	fmt.Printf("Parsing completed. Time Elapesed: %.2f seconds\n", time.Since(t).Seconds())
//}
//
//func parseURL(url string) {
//	for i := 0; i < 5; i++ {
//		latency := rand.Intn(500) + 500
//		time.Sleep(time.Duration(latency) * time.Millisecond)
//		fmt.Printf("Parsing <%s> - Step %d - Latency %d ms\n", url, i+1, latency)
//	}
//}

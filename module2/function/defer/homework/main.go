package main

import (
	"errors"
	"fmt"
)

var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

func main() {
	fmt.Println(ExecuteMergeDictsJob(&MergeDictsJob{}))
	fmt.Println(ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"}, nil}}))
	fmt.Println(ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"}, {"b": "c"}}}))
}

func ExecuteMergeDictsJob(job *MergeDictsJob) (MergeDictsJob, error) {
	if len(job.Dicts) < 2 {
		return *job, errNotEnoughDicts
	}
	for _, dict := range job.Dicts {
		if dict == nil {
			return *job, errNilDict
		}
		for key, value := range dict {
			job.Merged[key] = value
		}
	}
	job.IsFinished = true
	return *job, nil

}

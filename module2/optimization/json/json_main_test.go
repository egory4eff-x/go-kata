package main

import (
	"encoding/json"
	jsoniter "github.com/json-iterator/go"
	"testing"
)

type Welcome []WelcomeElement

func UnmarshalWelcome(data []byte) (Welcome, error) {
	var w Welcome
	err := json.Unmarshal(data, &w)
	return w, err
}

func (w *Welcome) Marshal() ([]byte, error) {
	return json.Marshal(w)
}

type WelcomeElement struct {
	ID        int64      `json:"id"`
	Category  Category   `json:"category"`
	Name      string     `json:"name"`
	PhotoUrls []string   `json:"photoUrls"`
	Tags      []Category `json:"tags"`
	Status    string     `json:"status"`
}

type Category struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

func UnmarshalWelcome2(data []byte) (Welcome, error) {
	var w Welcome
	err := jsoniter.Unmarshal(data, &w)
	return w, err
}

func (w *Welcome) Marshal2() ([]byte, error) {
	return jsoniter.Marshal(w)
}

func BenchmarkStandart(b *testing.B) {

	jsonData := []byte(`[
        {
            "id": 1,
            "category": {
                "id": 1,
                "name": "Category 1"
            },
            "name": "Name 1",
            "photoUrls": ["url1", "url2"],
            "tags": [
                {
                    "id": 2,
                    "name": "Tag 1"
                }
            ],
            "status": "available"
        }
    ]`)

	b.ResetTimer() //исколючаем время на аллокацию переменных
	for i := 0; i < b.N; i++ {
		welcome, err := UnmarshalWelcome(jsonData)
		if err != nil {
			panic(err)
		}
		data, err := welcome.Marshal()
		if err != nil {
			panic(err)
		}
		_ = data
	}
}

func BenchmarkJsonIter(b *testing.B) {
	var (
		welcome Welcome
		err     error
		data    []byte
	)

	jsonData := []byte(`[
        {
            "id": 1,
            "category": {
                "id": 1,
                "name": "Category 1"
            },
            "name": "Name 1",
            "photoUrls": ["url1", "url2"],
            "tags": [
                {
                    "id": 2,
                    "name": "Tag 1"
                }
            ],
            "status": "available"
        }
    ]`)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		welcome, err = UnmarshalWelcome2(jsonData)
		if err != nil {
			panic(err)
		}
		data, err = welcome.Marshal2()
		if err != nil {
			panic(err)
		}
		_ = data
	}

}

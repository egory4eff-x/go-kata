package task20

func kidsWithCandies(candies []int, extraCandies int) []bool {
	f := make([]bool, len(candies))
	maxCandies := 0

	for _, v := range candies {
		if v > maxCandies {
			maxCandies = v
		}
	}

	for i, v := range candies {
		if v+extraCandies >= maxCandies {
			f[i] = true
		}
	}
	return f
}

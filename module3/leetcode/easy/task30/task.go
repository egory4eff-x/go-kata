package task30

func countGoodTriplets(arr []int, a int, b int, c int) (count int) {
	n := len(arr)
	for i := 0; i < n-2; i++ {
		for j := i + 1; j < n-1; j++ {
			if !isGood(a, arr[i], arr[j]) {
				continue
			}

			for k := j + 1; k < n; k++ {
				if isGood(b, arr[j], arr[k]) && isGood(c, arr[i], arr[k]) {
					count++
				}
			}
		}
	}

	return
}

func isGood(t, a, b int) bool {
	return abs(a-b) <= t
}

func abs(n int) int {
	if n < 0 {
		return -1 * n
	}
	return n
}

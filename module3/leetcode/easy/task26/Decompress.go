package task26

func decompressRLElist(nums []int) []int {
	s := []int{}

	for i := 0; i < len(nums)-1; i = i + 2 {
		fred := nums[i]
		val := nums[i+1]

		arr := make([]int, fred)
		for i := range arr {
			arr[i] = val
		}
		s = append(s, arr...)
	}
	return s
}

package task13

func numJewelsInStones(jewels string, stones string) int {
	count := 0

	jewelsMap := map[rune]bool{}

	for _, jewel := range jewels {
		jewelsMap[jewel] = true
	}

	for _, stone := range stones {
		if jewelsMap[stone] == true {
			count++
		}
	}
	return count
}

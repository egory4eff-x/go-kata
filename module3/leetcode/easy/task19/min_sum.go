package task19

import "sort"

func minimumSum(num int) int {
	numbers := []int{}

	for num > 0 {
		numbers = append(numbers, num%10)
		num /= 10
	}

	sort.Ints(numbers)

	num1 := numbers[0]*10 + numbers[2]
	num2 := numbers[1]*10 + numbers[3]

	return num1 + num2
}

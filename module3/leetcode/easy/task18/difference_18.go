package task18

func differenceOfSum(nums []int) int {
	result := 0
	for _, i := range nums {
		result += i
		for i > 0 {
			result -= i % 10
			i = i / 10
		}
	}
	return result
}

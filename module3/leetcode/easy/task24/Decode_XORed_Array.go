package task24

func decode(encoded []int, first int) []int {
	n := len(encoded)
	arr := make([]int, n+1)
	arr[0] = first
	prev := first

	for i := 0; i < n; i++ {
		arr[i+1] = prev ^ encoded[i]
		prev = arr[i+1]
	}
	return arr
}

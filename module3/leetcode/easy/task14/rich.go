package task14

func maximumWealth(accounts [][]int) int {
	max := 0
	for _, i := range accounts {
		count := 0
		for _, j := range i {
			count += j
		}
		if max < count {
			max = count
		}
	}
	return max
}

package task23

import "strings"

func interpret(command string) string {
	for i := 0; i < len(command); i++ {
		command = strings.Replace(command, "()", "o", 1)
		command = strings.Replace(command, "(al)", "al", 1)
	}
	return command
}

package task10

func shuffle(nums []int, n int) []int {
	s1 := nums[:n]
	s2 := nums[n:]
	s3 := make([]int, 0)

	for i := 0; i < n; i++ {
		s3 = append(s3, s1[i])
		s3 = append(s3, s2[i])
	}
	return s3
}

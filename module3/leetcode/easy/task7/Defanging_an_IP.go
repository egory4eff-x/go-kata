package task7

import "strings"

func defangIPaddr(address string) string {
	f := strings.Replace(address, ".", "[.]", 3)
	return f
}

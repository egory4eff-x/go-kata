package task9

func finalValueAfterOperations(operations []string) int {
	fin := map[string]int{
		"--X": -1,
		"X--": -1,
		"X++": 1,
		"++X": 1,
	}
	X := 0

	for _, i := range operations {
		X += fin[i]
	}
	return X
}

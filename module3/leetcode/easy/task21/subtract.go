package task21

func subtractProductAndSum(n int) int {
	result := 0
	sum := 0
	pro := 1
	num := []int{}
	for n > 0 {
		num = append(num, n%10)
		n /= 10
	}
	for _, i := range num {
		sum += i
	}
	for _, i := range num {
		pro *= i
	}
	result = pro - sum
	return result
}

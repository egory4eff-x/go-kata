package task5

func numberOfMatches(n int) int {
	count_player := 0
	count_match := 0

	for n > 1 {
		if n%2 == 0 {
			count_player = n / 2
			n /= 2
		} else {
			count_player = (n - 1) / 2
			n = ((n - 1) / 2) + 1
		}
		count_match += count_player
	}
	return count_match
}

package task28

func countDigits(num int) int {
	res := 0
	n := num
	for n > 0 {
		val := n % 10
		if num%val == 0 {
			res++
		}
		n = (n - val) / 10
	}
	return res
}

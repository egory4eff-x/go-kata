package task11

func runningSum(nums []int) []int {
	sum := 0
	s := make([]int, len(nums))

	for i, v := range nums {
		sum += v
		s[i] = sum
	}
	return s
}

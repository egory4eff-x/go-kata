package task25

func createTargetArray(nums []int, index []int) []int {
	s := make([]int, len(nums))

	for idx, i := range index {
		for j := len(nums) - 1; j > i; j-- {
			s[j] = s[j-1]
		}
		s[i] = nums[idx]
	}
	return s
}

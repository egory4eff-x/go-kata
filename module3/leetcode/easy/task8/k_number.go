package task8

func findKthPositive(arr []int, k int) int {
	// Определяем максимальное значение, которое может быть пропущено
	maxMissing := arr[len(arr)-1] - len(arr)
	if k > maxMissing {
		// Если k больше максимального пропущенного значения,
		// k-е пропущенное число находится за пределами массива arr
		return arr[len(arr)-1] + (k - maxMissing)
	}

	left, right := 0, len(arr)-1
	// Используем бинарный поиск для поиска k-го пропущенного числа
	for left < right {
		mid := (left + right) / 2
		missing := arr[mid] - mid - 1

		if missing < k {
			// Если количество пропущенных чисел меньше k, продолжаем поиск в правой половине диапазона
			left = mid + 1
		} else {
			// Если количество пропущенных чисел больше или равно k, продолжаем поиск в левой половине диапазона
			right = mid
		}
	}

	return left + k
}

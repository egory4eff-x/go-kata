package task1

func tribonacci(n int) int {
	memo := [3]int{0, 1, 1}
	if n < 3 {
		return memo[n]
	}

	for i := 0; i <= n; i++ {
		memo[0], memo[1], memo[2] = memo[1], memo[2], memo[0]+memo[1]+memo[2]
	}
	return memo[2]
}

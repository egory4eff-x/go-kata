package task22

func smallerNumbersThanCurrent(nums []int) []int {
	res := make([]int, len(nums))
	for i := 0; i < len(nums); i++ {
		var cnt int
		for _, num := range nums {
			if num < nums[i] {
				cnt++
			}
		}
		res[i] = cnt
	}
	return res
}

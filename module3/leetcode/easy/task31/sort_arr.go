package task31

import "sort"

func sortPeople(names []string, heights []int) []string {
	m := map[int]string{}
	for i := 0; i < len(names); i++ {
		m[heights[i]] = names[i]
	}
	sort.Slice(heights, func(i, j int) bool {
		return heights[j] < heights[i]
	})

	ans := make([]string, 0, len(m))
	for j := 0; j < len(m); j++ {
		ans = append(ans, m[heights[j]])
	}

	return ans
}

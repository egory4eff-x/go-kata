package task17

import "strings"

func mostWordsFound(sentences []string) int {
	count := 0
	for _, proposal := range sentences {
		word := strings.Count(proposal, " ") + 1

		if word > count {
			count = word
		}
	}
	return count
}

package main

import "fmt"

func groupThePeople(groupSizes []int) [][]int {
	var groups [][]int
	gSizes := make(map[int][]int)

	for idx, val := range groupSizes {
		gSizes[val] = append(gSizes[val], idx)

		//если длина среза равна текущему значению
		if len(gSizes[val]) == val {
			groups = append(groups, gSizes[val])
			gSizes[val] = nil
		}
	}
	for _, v := range gSizes {
		if v != nil {
			groups = append(groups, v)
		}
	}
	return groups
}

func main() {
	m := []int{2, 3, 4, 2, 3, 4, 6}
	fmt.Println(groupThePeople(m))
}

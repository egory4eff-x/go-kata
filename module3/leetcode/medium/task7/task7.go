package task7

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func balanceBST(root *TreeNode) *TreeNode {
	nodes := []*TreeNode{} //создаем пустой срез
	dfs(root, &nodes)      //передаем в функцию новый срез и старое дерево
	return balance(nodes)  //передаем в функцию новый срез
}

// функция баланса дерева
func balance(nodes []*TreeNode) *TreeNode {
	n := len(nodes) //создали переменную длины среза
	if n == 0 {
		return nil
	}
	if n == 1 {
		nodes[0].Left, nodes[0].Right = nil, nil
		return nodes[0]
	}
	newRoot := nodes[n/2]                  //объявляем переменую значение ноды(узла)
	newRoot.Left = balance(nodes[:n/2])    //переменная левых ребер(четные)
	newRoot.Right = balance(nodes[n/2+1:]) //переменная правых ребер(нечетные)
	return newRoot
}

// функция добавления элементов, в новое сбалансированное дерево
func dfs(node *TreeNode, nodes *[]*TreeNode) {
	if node == nil {
		return
	}
	if node.Left != nil {
		dfs(node.Left, nodes)
	}
	*nodes = append(*nodes, node)
	if node.Right != nil {
		dfs(node.Right, nodes)
	}
}

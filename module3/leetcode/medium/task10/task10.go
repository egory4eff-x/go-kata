package task10

import "strings"

func numTilePossibilities(tiles string) int {
	seqs := map[string]bool{} //создали массив
	t := strings.Split(tiles, "")
	n := len(tiles)
	done := make([]bool, n)

	var dfs func(cur string, size int)
	dfs = func(cur string, size int) {
		if len(cur) == size {
			seqs[cur] = true
			return
		}
		for i := 0; i < n; i++ {
			if done[i] == false {
				done[i] = true
				dfs(cur+t[i], size)
				done[i] = false
			}
		}
		return
	}

	for size := 1; size <= n; size++ {
		dfs("", size)
	}
	return len(seqs)
}

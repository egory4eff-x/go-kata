package task12

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeInBetween(list1 *ListNode, a int, b int, list2 *ListNode) *ListNode {

	list1Iterator, list1IteratorPrev, list1Counter := list1, list1, 0
	list2Iterator, list2End := list2, list2

	for list2Iterator != nil {
		list2End = list2Iterator
		list2Iterator = list2Iterator.Next
	}

	for list1Iterator != nil {

		if list1Counter == a {
			list1IteratorPrev.Next = list2
		}

		if list1Counter == b {
			list2End.Next = list1Iterator.Next
			break
		}

		list1Counter++

		list1IteratorPrev = list1Iterator
		list1Iterator = list1Iterator.Next
	}

	return list1
}

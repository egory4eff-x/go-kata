package task3

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeNodes(head *ListNode) *ListNode {
	runner := head.Next

	for runner != nil {
		for runner.Next.Val != 0 {
			runner.Val = runner.Next.Val
			runner.Next = runner.Next.Next
		}
		runner.Next = runner.Next.Next
		runner = runner.Next
	}
	return head.Next
}

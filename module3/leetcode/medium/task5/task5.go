package task5

import "math"

type ListNode struct {
	Val  int
	Next *ListNode
}

func pairSum(head *ListNode) int {
	result := 0

	//middle of the node
	slow := head
	fast := head
	for fast != nil && fast.Next != nil {
		slow = slow.Next
		fast = fast.Next.Next
	}

	//reverse middle of the node
	var prev *ListNode
	for slow != nil {
		nextNode := slow.Next
		slow.Next = prev
		prev = slow
		slow = nextNode
	}

	//compare the head to the middle of the node
	//to get the max twin sum
	for head != nil && prev != nil {
		val := head.Val + prev.Val
		result = int(math.Max(float64(result), float64(val)))
		head = head.Next
		prev = prev.Next
	}
	return result
}

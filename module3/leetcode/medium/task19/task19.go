package task19

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

type pair struct {
	first  int
	second int
}

func solve(root *TreeNode, ans *int) pair {
	if root == nil {
		return pair{0, 0}
	}
	l := solve(root.Left, ans)
	r := solve(root.Right, ans)
	sum := l.first + r.first + root.Val
	count := l.second + r.second + 1
	if sum/count == root.Val {
		*ans = *ans + 1
	}
	return pair{sum, count}
}
func averageOfSubtree(root *TreeNode) int {
	var ans int
	solve(root, &ans)
	return ans
}

package task13

func maxSum(grid [][]int) int {
	var m int = len(grid)
	var n int = len(grid[0])
	for i := 0; i < m; i++ {
		for j := 1; j < n; j++ {
			grid[i][j] += grid[i][j-1]
		}
	}
	var ans int = 0
	for i := 2; i < m; i++ {
		var sum int = 0
		for j := 2; j < n; j++ {
			sum = grid[i][j] + grid[i-2][j] + grid[i-1][j-1] - grid[i-1][j-2]
			if j > 2 {
				sum -= grid[i][j-3] + grid[i-2][j-3]
			}
			if sum > ans {
				ans = sum
			}
		}
	}
	return ans
}

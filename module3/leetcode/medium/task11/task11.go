package task11

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func removeLeafNodes(root *TreeNode, target int) *TreeNode {
	if root == nil {
		return nil
	}
	// Рекурсивный вызов функции для удаления листьев в левом и правом поддереве
	root.Left = removeLeafNodes(root.Left, target)
	root.Right = removeLeafNodes(root.Right, target)
	// Если узел является листовым и содержит значение target, он будет удален
	if root.Left == nil && root.Right == nil && root.Val == target {
		return nil
	}
	return root
}

package task20

func processQueries(queries []int, m int) []int {
	numbers := make([]int, m)
	for i := range numbers {
		numbers[i] = i + 1
	}
	queryResult := make([]int, 0, len(queries))
	for _, q := range queries {
		for i, v := range numbers {
			if v == q {
				queryResult = append(queryResult, i)
				copy(numbers[1:i+1], numbers[:i])
				numbers[0] = v
			}
		}
	}
	return queryResult
}

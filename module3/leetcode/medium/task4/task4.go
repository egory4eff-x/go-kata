package task4

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func bstToGst(root *TreeNode) *TreeNode {
	if root == nil {
		return root
	}
	sum := 0
	convert(root, &sum)
	return root

}

func convert(root *TreeNode, sum *int) {
	if root == nil {
		return
	}

	convert(root.Right, sum)
	*sum += root.Val
	root.Val = *sum
	convert(root.Left, sum)
	return
}

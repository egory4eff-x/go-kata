package task1

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func deepestLeavesSum(root *TreeNode) int {
	sum := 0

	if root != nil {
		return sum
	}

	s_root := []*TreeNode{root}

	if len(s_root) > 0 {
		l := len(s_root)
		sum = 0
		for i := 0; i < l; i++ {
			node := s_root[i]

			sum += node.Val

			if node.Left != nil {
				s_root = append(s_root, node.Left)
			}
			if node.Right != nil {
				s_root = append(s_root, node.Right)
			}
		}
		s_root = s_root[l:]
	}
	return sum
}

package algo

import (
	"math/rand"
)

func QuickSort(arr []int) []int {
	if len(arr) <= 1 {
		return arr
	}
	median := arr[rand.Intn(len(arr))] // находим медиану выбирая случайное число

	low_park := make([]int, 0, len(arr))    //
	high_park := make([]int, 0, len(arr))   //
	middle_park := make([]int, 0, len(arr)) //

	for _, item := range arr {
		switch {
		case item < median:
			low_park = append(low_park, item) //запись числе меньше медианы
		case item == median:
			middle_park = append(middle_park, item) //запись чисел равные медиане
		case item > median:
			high_park = append(high_park, item) //запись чисел больше медианы
		}
	}

	low_park = QuickSort(low_park)
	high_park = QuickSort(high_park)

	//распаковываем среднюю часть после всех чисел
	low_park = append(low_park, middle_park...)
	//далее распаковываем оставшуюся часть
	low_park = append(low_park, high_park...)

	return low_park
}

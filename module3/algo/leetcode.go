package algo

func minimumAverageDifference(nums []int) int {
	//создадим переменную для суммы справа
	var sumRight int
	//пройдемся один раз для вычисления общей суммы
	for i := range nums {
		sumRight += nums[i]
	}

	minDiff := 9223372036854775807 //задаем максимальное значение int64
	var idx int                    //индекс для ответа
	var sumLeft int                // для вычисления суммы слева
	rightCount := len(nums)        //заранее вычислим кол-во справа
	var avgDiff int
	for i := 0; i < len(nums); i++ {
		sumLeft += nums[i]   //добавляем по одному элементу слева
		sumRight -= nums[i]  //добавляем по одному элементу справа
		rightCount--         //уменьшаем кол-во элементов справа
		if rightCount == 0 { //защита от 0
			rightCount++
		}
		//вычисляем среднюю разницу
		avgDiff = sumLeft/(i+1) - sumRight/rightCount
		//наш модуль
		if avgDiff < 0 {
			avgDiff *= -1
		}
		//если наше минимальное значение больше вычисленного
		if avgDiff < minDiff {
			minDiff = avgDiff //устанавливаем его в наш диф
			idx = i           //записываем индекс для выдачи ответа
		}
	}

	//возвращаем временно 0
	return idx
}

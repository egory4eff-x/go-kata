package task

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math/rand"
	"time"

	"github.com/brianvoe/gofakeit"
)

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	LoadData(path string) error
	Insert(n int, c Commit) error
	Delete(n int) error
	DeleteCurrent() error
	Index()
	Pop()
	Shift()
	SearchUUID(uuID string) *Node
	Search(message string) *Node
	Reverse() *DoubleLinkedList
}

// LoadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) error {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}
	var commits []Commit
	if err := json.Unmarshal([]byte(data), &commits); err != nil {
		return err
	}
	commits = QuickSort(commits)

	nodes := make([]*Node, len(commits))

	for i, commit := range commits {
		node := &Node{
			data: &Commit{
				Message: commit.Message,
				UUID:    commit.UUID,
				Date:    commit.Date,
			},
			prev: nil,
			next: nil,
		}
		nodes[i] = node

		for i, node := range nodes {
			if i == 0 {
				d.head = node
				d.tail = node
			} else {
				node.prev = d.tail
				d.tail.next = node
				d.tail = node
			}
			d.len++
		}
		d.curr = d.head
	}
	return nil
}

func QuickSort(commits []Commit) []Commit {
	if len(commits) < 2 {
		return commits
	}
	rand.Seed(time.Now().UnixNano())
	median := commits[rand.Intn(len(commits))]

	left := make([]Commit, len(commits))
	mid := make([]Commit, len(commits))
	right := make([]Commit, len(commits))
	for _, val := range commits {
		switch {
		case val.Date.Before(median.Date):
			left = append(left, val)
		case val.Date.Equal(median.Date):
			mid = append(mid, val)
		case val.Date.After(median.Date):
			right = append(right, val)
		}
	}
	left = QuickSort(left)
	right = QuickSort(right)

	left = append(left, mid...)
	left = append(left, right...)
	return left

}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	if d.curr == nil {
		return nil
	}
	next := d.curr.next
	d.curr = next
	return d.curr

}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	if d.curr == nil {
		return nil
	}
	return d.curr.prev

}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	if n < 0 || n > d.len {
		return errors.New("Индекс не соответствует диапазону")
	}
	NewNode := &Node{data: &c}
	if n == d.len && n != 0 {
		NewNode.prev = d.tail
		d.tail.next = NewNode
		d.tail = NewNode
		return nil
	}
	if n == d.Len() && n == 0 {
		d.head = NewNode
		d.tail = NewNode
		return nil
	} else {
		node := d.head
		for d.len != n {
			node = node.next
		}

	}
	NextNode := d.tail.next
	d.tail.next = NewNode
	NewNode.prev = d.tail
	NextNode.prev = NewNode
	NewNode.next = NextNode
	d.len++
	return nil
}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {
	if n <= 0 && n >= d.len {
		return errors.New("Индекс не соответствует диапазону")
	}
	if n == 1 {
		d.head = d.head.next
		d.head.prev = nil
		d.len--
		return nil
	} else if n == d.len {
		d.tail = d.tail.prev
		d.tail.next = nil
		d.len--
		return nil
	}
	node := d.head
	for d.len != n {
		node = node.next
	}
	left := d.tail.prev
	right := d.tail.next
	left.next = right
	right.prev = left
	d.len--
	return nil
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {
	if d.curr == nil {
		return fmt.Errorf("текущий элемент равен nil")
	} else if d.curr == d.head {
		d.head = d.head.next
		d.head.prev = nil
		d.curr = d.head

		d.len--
		return nil
	} else if d.curr == d.tail {
		d.tail = d.tail.prev
		d.tail.next = nil
		d.curr = d.tail

		d.len--
		return nil
	}
	d.curr.prev.next = d.curr.next
	d.curr.next.prev = d.curr.prev

	d.len--
	return nil

}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	if d.curr == nil {
		return -1, fmt.Errorf("Текущий элемент равен nil")
	}
	index := 0
	node := d.head
	for node != d.curr {
		index++
		node = node.next
	}
	return index, nil
}

// Pop Операция Pop удаление последнего элемента массива
func (d *DoubleLinkedList) Pop() *Node {
	if d.tail == nil {
		return nil
	}
	node := d.tail
	d.tail = node.prev
	d.tail.next = nil
	d.len--
	return node

}

// Shift операция shift
func (d *DoubleLinkedList) Shift() *Node {
	if d.head == nil {
		return nil
	}
	node := d.head
	d.head = node.next
	d.head.prev = nil
	d.len--
	return node
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	if d.head == nil {
		return nil
	}
	node := d.head
	for node.data.UUID != uuID {
		node = node.next
	}
	d.curr = node
	return d.curr
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) *Node {
	if d.head == nil {
		return nil
	}
	node := d.head
	for node.data.Message != message {
		node = node.next
	}
	d.curr = node
	return d.curr
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {

	node := d.head
	var prev *Node

	for node != nil {
		next := node.next
		node.next = prev
		node.prev = next
		prev = node
		node = next
	}
	d.head, d.tail = d.tail, d.head
	return d
}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

func GenerateJSON(numCommits int) []Commit {
	gofakeit.Seed(time.Now().UnixNano())
	var commits []Commit

	for i := 0; i < numCommits; i++ {
		commit := Commit{
			Message: gofakeit.Sentence(10),
			UUID:    gofakeit.UUID(),
			Date:    gofakeit.DateRange(time.Now().Add(-time.Hour*24*365), time.Now()),
		}

		commits = append(commits, commit)
	}

	return commits
}

func GenData(n int) (*DoubleLinkedList, error) {
	d := &DoubleLinkedList{}
	commits := GenerateJSON(n)
	commits = QuickSort(commits)
	nodes := make([]*Node, len(commits))

	for i, commit := range commits {
		node := &Node{data: &Commit{
			Message: commit.Message,
			UUID:    commit.UUID,
			Date:    commit.Date,
		}}
		nodes[i] = node
	}

	for i, node := range nodes {
		if i == 0 {
			d.head = node
			d.tail = node
		} else {
			node.prev = d.tail
			d.tail.next = node
			d.tail = node
		}
		d.len++
	}

	d.curr = d.head
	return d, nil
}

package main

import (
	"fmt"
)

// Интерфейс PricingStrategy
type PricingStrategy interface {
	Calculate(order Order) float64
}

// Структура RegularPricing
type RegularPricing struct{}

// Реализация метода Calculate для RegularPricing
func (rp RegularPricing) Calculate(order Order) float64 {
	return order.Price
}

// Структура SalePricing
type SalePricing struct {
	Discount     float64
	DiscountName string
}

// Реализация метода Calculate для SalePricing
func (sp SalePricing) Calculate(order Order) float64 {
	discountedPrice := order.Price * (1 - sp.Discount/100)
	return discountedPrice
}

// Структура Order
type Order struct {
	ProductName string
	Price       float64
	Quantity    int
}

func main() {
	// Создание экземпляров структур
	regular := RegularPricing{}
	sale1 := SalePricing{Discount: 5, DiscountName: "весенняя скидка"}
	sale2 := SalePricing{Discount: 10, DiscountName: "скидка регулярного клиента"}
	sale3 := SalePricing{Discount: 15, DiscountName: "другая скидка"}

	// Создание слайса из интерфейсов PricingStrategy
	pricingStrategies := []PricingStrategy{regular, sale1, sale2, sale3}

	// Создание заказа
	order := Order{
		ProductName: "Название товара",
		Price:       100.0,
		Quantity:    1,
	}

	// Вычисление цены для каждой стратегии и вывод результатов
	for _, strategy := range pricingStrategies {
		calculatedPrice := strategy.Calculate(order)
		fmt.Printf("Стратегия: %T\n", strategy)
		fmt.Printf("Цена со скидкой: %.2f\n\n", calculatedPrice)
	}
}

package main

import "fmt"

// Интерфейс AirConditioner определяет методы для управления кондиционером
type AirConditioner interface {
	TurnOn()
	TurnOff()
	SetTemperature(temperature int)
}

// Реализация RealAirConditioner, представляющего реальный кондиционер
type RealAirConditioner struct {
	temperature int
}

func (ac *RealAirConditioner) TurnOn() {
	fmt.Println("Кондиционер включен")
}

func (ac *RealAirConditioner) TurnOff() {
	fmt.Println("Кондиционер выключен")
}

func (ac *RealAirConditioner) SetTemperature(temperature int) {
	fmt.Printf("Установлена температура: %d\n", temperature)
	ac.temperature = temperature
}

// Структура AirConditionerAdapter адаптирует интерфейс RealAirConditioner к интерфейсу AirConditioner
type AirConditionerAdapter struct {
	airConditioner *RealAirConditioner
}

func (adapter *AirConditionerAdapter) TurnOn() {
	adapter.airConditioner.TurnOn()
}

func (adapter *AirConditionerAdapter) TurnOff() {
	adapter.airConditioner.TurnOff()
}

func (adapter *AirConditionerAdapter) SetTemperature(temperature int) {
	adapter.airConditioner.SetTemperature(temperature)
}

// Структура AirConditionerProxy представляет собой прокси-объект для AirConditionerAdapter
type AirConditionerProxy struct {
	adapter       *AirConditionerAdapter
	authenticated bool
}

func (proxy *AirConditionerProxy) TurnOn() {
	if proxy.authenticated {
		proxy.adapter.TurnOn()
	} else {
		fmt.Println("Ошибка: Нет прав доступа для включения кондиционера")
	}
}

func (proxy *AirConditionerProxy) TurnOff() {
	if proxy.authenticated {
		proxy.adapter.TurnOff()
	} else {
		fmt.Println("Ошибка: Нет прав доступа для выключения кондиционера")
	}
}

func (proxy *AirConditionerProxy) SetTemperature(temperature int) {
	if proxy.authenticated {
		proxy.adapter.SetTemperature(temperature)
	} else {
		fmt.Println("Ошибка: Нет прав доступа для установки температуры кондиционера")
	}
}

// Функция NewAirConditionerProxy создает новый объект AirConditionerProxy с заданным значением authenticated
func NewAirConditionerProxy(authenticated bool) *AirConditionerProxy {
	realAirConditioner := &RealAirConditioner{}
	airConditionerAdapter := &AirConditionerAdapter{airConditioner: realAirConditioner}
	return &AirConditionerProxy{adapter: airConditionerAdapter, authenticated: authenticated}
}

func main() {
	// Создание объектов AirConditionerProxy с разными значениями authenticated
	authorizedProxy := NewAirConditionerProxy(true)
	unauthorizedProxy := NewAirConditionerProxy(false)

	// Вызов методов интерфейса AirConditioner для каждого прокси
	authorizedProxy.TurnOn()
	authorizedProxy.SetTemperature(25)
	authorizedProxy.TurnOff()

	unauthorizedProxy.TurnOn()
	unauthorizedProxy.SetTemperature(20)
	unauthorizedProxy.TurnOff()
}

package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// WeatherAPI - интерфейс, определяющий методы для получения информации о погоде
type WeatherAPI interface {
	GetTemperature(location string) (float64, error)
	GetHumidity(location string) (float64, error)
	GetWindSpeed(location string) (float64, error)
}

// TomorrowAPI - реализация интерфейса WeatherAPI, использующая API от Tomorrow.io
type TomorrowAPI struct {
	apiKey string
}

type weatherResponse struct {
	Data struct {
		Temperature struct {
			Value float64 `json:"value"`
		} `json:"temperature"`
		Humidity struct {
			Value float64 `json:"value"`
		} `json:"humidity"`
		Wind struct {
			Speed struct {
				Value float64 `json:"value"`
			} `json:"speed"`
		} `json:"wind"`
	} `json:"data"`
}

func (t *TomorrowAPI) GetTemperature(location string) (float64, error) {
	url := fmt.Sprintf("https://api.tomorrow.io/v4/timelines?location=%s&fields=temperature_2m&units=metric&timesteps=current&apikey=%s", location, t.apiKey)
	resp, err := http.Get(url)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()

	var data weatherResponse
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return 0, err
	}

	return data.Data.Temperature.Value, nil
}

func (t *TomorrowAPI) GetHumidity(location string) (float64, error) {
	url := fmt.Sprintf("https://api.tomorrow.io/v4/timelines?location=%s&fields=humidity_2m&units=metric&timesteps=current&apikey=%s", location, t.apiKey)
	resp, err := http.Get(url)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()

	var data weatherResponse
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return 0, err
	}

	return data.Data.Humidity.Value, nil
}

func (t *TomorrowAPI) GetWindSpeed(location string) (float64, error) {
	url := fmt.Sprintf("https://api.tomorrow.io/v4/timelines?location=%s&fields=wind_speed_10m&units=metric&timesteps=current&apikey=%s", location, t.apiKey)
	resp, err := http.Get(url)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()

	var data weatherResponse
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return 0, err
	}

	return data.Data.Wind.Speed.Value, nil
}

// WeatherFacade - фасад, предоставляющий упрощенный интерфейс для работы с API погоды
type WeatherFacade struct {
	weatherAPI WeatherAPI
}

// GetWeatherInfo - метод фасада для получения информации о погоде
func (w *WeatherFacade) GetWeatherInfo(location string) (float64, float64, float64, error) {
	temperature, err := w.weatherAPI.GetTemperature(location)
	if err != nil {
		return 0, 0, 0, err
	}

	humidity, err := w.weatherAPI.GetHumidity(location)
	if err != nil {
		return 0, 0, 0, err
	}

	windSpeed, err := w.weatherAPI.GetWindSpeed(location)
	if err != nil {
		return 0, 0, 0, err
	}

	return temperature, humidity, windSpeed, nil
}

// NewWeatherFacade - функция для создания нового экземпляра WeatherFacade
func NewWeatherFacade(apiKey string) *WeatherFacade {
	return &WeatherFacade{
		weatherAPI: &TomorrowAPI{apiKey: apiKey},
	}
}

func main() {
	apiKey := "3a4bhkGza3Klyns2JjA2mJMV0l5RPyHx"

	weatherFacade := NewWeatherFacade(apiKey)
	cities := []string{"Moscow", "St. Petersburg", "Kazan", "Yakutsk"}

	for _, city := range cities {
		temperature, humidity, windSpeed, err := weatherFacade.GetWeatherInfo(city)
		if err != nil {
			fmt.Printf("Failed to get weather for %s: %v\n", city, err)
			continue
		}

		fmt.Printf("Temperature in %s: %.1f°C\n", city, temperature)
		fmt.Printf("Humidity in %s: %.1f%%\n", city, humidity)
		fmt.Printf("Wind speed in %s: %.1f m/s\n\n", city, windSpeed)
	}
}

package main

import (
	"errors"
	"fmt"
	"time"
)

type Post struct {
	body        string //текст поста
	publishDate int64  //дата публикации поста в формате Unix timestamp
	next        *Post  // указатель на следующий пост в потоке
}

type Feed struct {
	length int   //кол-во постов в потоке
	start  *Post //указатель на первый пост в потоке
	end    *Post //указатель на последний пост в потоке
}

// Реализуйте метод Append для добавления нового поста в конец потока
func (f *Feed) Append(newPost *Post) {
	if f.length == 0 { //если кол-во постов равно 0

		//новый пост будет и началом и концом ленты
		f.start = newPost
		f.end = newPost
	} else { //если в ленте уже есть посты

		//добавляем новый пост в конец ленты
		lastPost := f.end
		lastPost.next = newPost
		f.end = newPost
	}

	//добавляем каждый новый поток
	f.length++
}

// Реализуйте метод Remove для удаления поста по дате публикации из потока
func (f *Feed) Remove(publishDate int64) {
	if f.length == 0 {
		panic(errors.New("Лента - пуста"))
	}

	var previousPost *Post
	currentPost := f.start

	for currentPost.publishDate != publishDate {
		if currentPost.next == nil {
			panic(errors.New("Пост не найден"))
		}

		previousPost = currentPost
		currentPost = currentPost.next
	}
	previousPost.next = currentPost.next

	f.length--
}

// Реализуйте метод Insert для вставки нового поста в поток в соответствии с его датой публикации
func (f *Feed) Insert(newPost *Post) {
	if f.length == 0 {
		f.start = newPost
	} else {
		var previousPost *Post
		currentPost := f.start

		for currentPost.publishDate < newPost.publishDate {
			previousPost = currentPost
			currentPost = previousPost.next
		}

		previousPost.next = newPost
		newPost.next = currentPost
	}
	f.length++
}

// Реализуйте метод Inspect для вывода информации о потоке и его постах
func (f *Feed) Inspect() {
	if f.length == 0 {
		fmt.Println("Лента - пуста")
	}
	fmt.Println("=======================")
	fmt.Println("Количество постов в ленте:", f.length)

	currentIndex := 0
	currentPost := f.start

	for currentIndex < f.length {
		fmt.Printf("Item: %v - %v\n", currentIndex, currentPost)
		currentPost = currentPost.next
		currentIndex++
	}
	fmt.Println("=======================")
}

func main() {
	rightNow := time.Now().Unix()
	f := &Feed{}
	p1 := &Post{
		body:        "Lorem ipsum",
		publishDate: rightNow,
	}
	p2 := &Post{
		body:        "Dolor sit amet",
		publishDate: rightNow + 10,
	}
	p3 := &Post{
		body:        "consectetur adipiscing elit",
		publishDate: rightNow + 20,
	}
	p4 := &Post{
		body:        "sed do eiusmod tempor incididunt",
		publishDate: rightNow + 30,
	}
	f.Append(p1)
	f.Append(p2)
	f.Append(p3)
	f.Append(p4)

	f.Inspect()

	newPost := &Post{
		body:        "Это новый пост:",
		publishDate: rightNow + 15,
	}
	f.Insert(newPost)
	f.Inspect()
}

package main

import (
	"bufio"
	"fmt"
	"gitlab.com/egory4eff-x/go-kata/module3/clean_architecture/cli/repository"
	"gitlab.com/egory4eff-x/go-kata/module3/clean_architecture/cli/service"
	useCli "gitlab.com/egory4eff-x/go-kata/module3/clean_architecture/cli/userCli"
	"os"
	"strconv"
	"strings"
)

func main() {
	repo := repository.NewInMemoryTodoRepository()
	todoApp := service.NewTodoApp(repo)
	cliApp := useCli.NewCliApp(todoApp)

	cliApp.Run()
}

func exitWithError(err error) {
	fmt.Println("Error:", err)
	os.Exit(1)
}

func readInput(prompt string) string {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print(prompt)
	input, _ := reader.ReadString('\n')
	return strings.TrimSpace(input)
}

func readIntInput(prompt string) int {
	input := readInput(prompt)
	num, err := strconv.Atoi(input)
	if err != nil {
		exitWithError(err)
	}
	return num
}

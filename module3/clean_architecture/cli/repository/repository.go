package repository

import (
	"errors"
	"gitlab.com/egory4eff-x/go-kata/module3/clean_architecture/cli/service"
	"sync"
)

type TodoRepository interface {
	Add(todo service.Todo) error
	GetAll() ([]service.Todo, error)
	Delete(id int) error
	Update(id int, text string) error
	Complete(id int) error
}

type InMemoryTodoRepository struct {
	todos  []service.Todo
	nextID int
	mu     sync.Mutex
}

func NewInMemoryTodoRepository() *InMemoryTodoRepository {
	return &InMemoryTodoRepository{
		todos:  make([]service.Todo, 0),
		nextID: 1,
	}
}

func (r *InMemoryTodoRepository) Add(todo service.Todo) error {
	r.mu.Lock()
	defer r.mu.Unlock()

	todo.ID = r.nextID
	r.nextID++

	r.todos = append(r.todos, todo)
	return nil
}

func (r *InMemoryTodoRepository) GetAll() ([]service.Todo, error) {
	r.mu.Lock()
	defer r.mu.Unlock()

	return r.todos, nil
}

func (r *InMemoryTodoRepository) Delete(id int) error {
	r.mu.Lock()
	defer r.mu.Unlock()

	index := r.findIndexByID(id)
	if index == -1 {
		return errors.New("todo item not found")
	}

	r.todos = append(r.todos[:index], r.todos[index+1:]...)
	return nil
}

func (r *InMemoryTodoRepository) Update(id int, text string) error {
	r.mu.Lock()
	defer r.mu.Unlock()

	index := r.findIndexByID(id)
	if index == -1 {
		return errors.New("todo item not found")
	}

	r.todos[index].Text = text
	return nil
}

func (r *InMemoryTodoRepository) Complete(id int) error {
	r.mu.Lock()
	defer r.mu.Unlock()

	index := r.findIndexByID(id)
	if index == -1 {
		return errors.New("todo item not found")
	}

	r.todos[index].Done = true
	return nil
}

func (r *InMemoryTodoRepository) findIndexByID(id int) int {
	for i, todo := range r.todos {
		if todo.ID == id {
			return i
		}
	}
	return -1
}

package service

import (
	"fmt"
	"gitlab.com/egory4eff-x/go-kata/module3/clean_architecture/cli/repository"
)

type Todo struct {
	ID   int
	Text string
	Done bool
}

type TodoApp struct {
	repo repository.TodoRepository
}

func NewTodoApp(repo repository.TodoRepository) *TodoApp {
	return &TodoApp{repo: repo}
}

func (s *TodoApp) AddTodoItem(text string) error {
	err := s.repo.Add(Todo{
		ID:   0,
		Text: text,
		Done: false,
	})
	if err != nil {
		return fmt.Errorf("Ошибка добавления задачи: %w", err)
	}
	return nil
}

func (s *TodoApp) GetTodos() ([]Todo, error) {
	todos, err := s.repo.GetAll()
	if err != nil {
		return nil, fmt.Errorf("Ошибка вывода задачи: %w", err)
	}
	return todos, nil
}

func (s *TodoApp) DeleteTodoItem(id int) error {
	err := s.repo.Delete(id)
	if err != nil {
		return fmt.Errorf("Ошибка удаления: %w", err)
	}
	return nil
}

func (s *TodoApp) EditTodoItem(id int, text string) error {
	err := s.repo.Update(id, text)
	if err != nil {
		return fmt.Errorf("Ошибка в изменении задачи: %w", err)
	}
	return nil
}

func (s *TodoApp) CompleteTodoItem(id int) error {
	err := s.repo.Complete(id)
	if err != nil {
		return fmt.Errorf("Ошибка вывода списка задач: %w", err)
	}
	return nil
}

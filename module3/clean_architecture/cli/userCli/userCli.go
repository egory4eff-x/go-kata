package useCli

import (
	"bufio"
	"fmt"
	"gitlab.com/egory4eff-x/go-kata/module3/clean_architecture/cli/service"
	"os"
	"strconv"
	"strings"
)

type CliApp struct {
	todoApp *service.TodoApp
}

func NewCliApp(todoApp *service.TodoApp) *CliApp {
	return &CliApp{todoApp: todoApp}
}

func (c *CliApp) Run() {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Приложение Todo CLI")
	fmt.Println("Доступные команды: список, добавить, удалить, редактировать, завершить, выйти")
	for {
		fmt.Print("Enter a command: ")
		scanner.Scan()
		command := scanner.Text()
		switch strings.ToLower(command) {
		case "Показать список задач":
			c.handleListCommand()
		case "Добавить задачу":
			c.handleAddCommand(scanner)
		case "Удалить задачу":
			c.handleDeleteCommand(scanner)
		case "Редактировать задачу по ID":
			c.handleEditCommand(scanner)
		case "Завершить задачу":
			c.handleCompleteCommand(scanner)
		case "Exit":
			fmt.Println("Спасибо, что восспользовались нашим приложением")
			return
		default:
			fmt.Println("Ошибка команды, попробуйте снова")
		}
	}
}

func (c *CliApp) handleListCommand() {
	todos, err := c.todoApp.GetTodos()
	if err != nil {
		fmt.Println("Error:", err)
		return
	}

	if len(todos) == 0 {
		fmt.Println("Нет задач")
		return
	}

	fmt.Println("Todo items:")
	for _, todo := range todos {
		status := "Incomplete"
		if todo.Done {
			status = "Complete"
		}
		fmt.Printf("- ID: %d, Text: %s, Status: %s\n", todo.ID, todo.Text, status)
	}
}

func (c *CliApp) handleAddCommand(scanner *bufio.Scanner) {
	fmt.Print("Enter todo item text: ")
	scanner.Scan()
	text := scanner.Text()
	err := c.todoApp.AddTodoItem(text)
	if err != nil {
		fmt.Println("Error:", err)
	} else {
		fmt.Println("Новая задача успешно добавлена.")
	}
}

func (c *CliApp) handleDeleteCommand(scanner *bufio.Scanner) {
	fmt.Print("Введите ID задачи для удаления: ")
	scanner.Scan()
	idStr := scanner.Text()
	id, err := strconv.Atoi(idStr)
	if err != nil {
		fmt.Println("Ошибка ID, попробуйте снова.")
		return
	}

	err = c.todoApp.DeleteTodoItem(id)
	if err != nil {
		fmt.Println("Error:", err)
	} else {
		fmt.Println("Задача успешно удалена")
	}
}

func (c *CliApp) handleEditCommand(scanner *bufio.Scanner) {
	fmt.Print("Введите ID задачи: ")
	scanner.Scan()
	idStr := scanner.Text()
	id, err := strconv.Atoi(idStr)
	if err != nil {
		fmt.Println("Ошибка ID. Попробуйте снова.")
		return
	}

	fmt.Print("Введите новый текст: ")
	scanner.Scan()
	text := scanner.Text()

	err = c.todoApp.EditTodoItem(id, text)
	if err != nil {
		fmt.Println("Error:", err)
	} else {
		fmt.Println("Задача успешно удалена")
	}
}

func (c *CliApp) handleCompleteCommand(scanner *bufio.Scanner) {
	fmt.Print("Enter ID of the todo item to complete: ")
	scanner.Scan()
	idStr := scanner.Text()
	id, err := strconv.Atoi(idStr)
	if err != nil {
		fmt.Println("Invalid ID. Please enter a valid integer.")
		return
	}

	err = c.todoApp.CompleteTodoItem(id)
	if err != nil {
		fmt.Println("Error:", err)
	} else {
		fmt.Println("Todo item marked as complete.")
	}
}

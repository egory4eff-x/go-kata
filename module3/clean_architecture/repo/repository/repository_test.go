package repository

import (
	"bytes"
	"testing"
)

func TestUserRepository_Find(t *testing.T) {
	file := new(bytes.Buffer)
	obj := NewUserRepository(file)

	users := []User{
		{ID: 1, Name: "Nina"},
		{ID: 2, Name: "Tom"},
		{ID: 3, Name: "Borya"},
	}

	if err := obj.Save(users); err != nil {
		t.Errorf("Ошибка сохранения")
	}

	result, err := obj.Find(1)
	if err != nil {
		t.Errorf("Поиск вернул ошибку")
	}

	foundUser, ok := result.(User)
	if !ok {
		t.Errorf("Полученный тип данных не User")
	}
	if foundUser.ID != users[0].ID || foundUser.Name != users[0].Name {
		t.Errorf("Пользователь не соответствует ожидаемому")

	}
	_, err = obj.Find(4)
	if err == nil {
		t.Errorf("User не найден")
	}

}
func TestUserRepository_FindAll(t *testing.T) {
	file := new(bytes.Buffer)
	obj := NewUserRepository(file)

	users := []User{
		{ID: 1, Name: "A"},
		{ID: 2, Name: "N"},
		{ID: 3, Name: "T"},
	}

	if err := obj.Save(users); err != nil {
		t.Errorf("Ошибка сохранения")
	}

	result, err := obj.FindAll()
	if err != nil || result == nil {
		t.Errorf("Ошибка возврата данных")
	}

}

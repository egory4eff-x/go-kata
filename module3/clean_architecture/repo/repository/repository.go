package repository

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
)

type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]interface{}, error)
}

type UserRepository struct {
	File io.ReadWriter
	file *bytes.Buffer
}

func NewUserRepository(file io.ReadWriter) *UserRepository {
	return &UserRepository{
		File: file,
	}
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// Загружать данные в конструкторе

func (r *UserRepository) Save(record interface{}) error {
	// logic to write a record as a JSON object to a file at r.FilePath
	File, err := json.Marshal(record)
	if err != nil {
		return err
	}
	if _, err := r.File.Write(File); err != nil {
		return err
	}
	return nil
}

func (r *UserRepository) Find(id int) (interface{}, error) {
	// logic to read a JSON object from a file at r.FilePath and return the corresponding record
	bufer := new(bytes.Buffer)
	_, err := io.Copy(bufer, r.File)
	if err != nil {
		return User{}, err
	}

	var users []User
	if err := json.Unmarshal(bufer.Bytes(), &users); err != nil {
		return User{}, err
	}
	for i, val := range users {
		if id == val.ID {
			return users[i], nil
		}
	}
	return User{}, fmt.Errorf("No ")

}

func (r *UserRepository) FindAll() ([]interface{}, error) {
	// logic to read all JSON objects from a file at r.FilePath and return a slice of records
	bufer := new(bytes.Buffer)
	_, err := io.Copy(bufer, r.File)
	if err != nil {
		return nil, err
	}
	var users []User
	if err := json.Unmarshal(bufer.Bytes(), &users); err != nil {
		return nil, err
	}
	usersInter := make([]interface{}, len(users))
	for i, user := range users {
		usersInter[i] = user
	}
	return usersInter, nil

}

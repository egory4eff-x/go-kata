package model

import "gitlab.com/egory4eff-x/go-kata/module3/clean_architecture/service/repo"

type Todo struct {
	ID          int         `json:"id"`
	Title       string      `json:"title"`
	Description string      `json:"description"`
	Status      string      `json:"status"`
	Tasks       []repo.Task `json:"tasks"`
	Completed   bool        `json:"completed"`
}

package repo

import (
	"encoding/json"
	"io"
	"os"
)

// Repository layer

// TodoRepository is a storage interface for to-do items
type TodoRepository interface {
	GetTasks() ([]Task, error)
	GetTask(id int) (Task, error)
	CreateTask(task Task) (Task, error)
	UpdateTask(task Task) (Task, error)
	DeleteTask(id int) error
}

// Task represents a to-do task
type Task struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Status      string `json:"status"`
}

// TaskRepository is a storage interface for tasks
type TaskRepository interface {
	GetTasks() ([]Task, error)
	GetTask(id int) (Task, error)
	CreateTask(task Task) (Task, error)
	UpdateTask(task Task) (Task, error)
	DeleteTask(id int) error
}

// FileTaskRepository is a file-based implementation of TaskRepository
type FileTaskRepository struct {
	FilePath string
}

// GetTasks returns all tasks from the storage
func (repo *FileTaskRepository) GetTasks() ([]Task, error) {
	var tasks []Task

	file, err := os.Open(repo.FilePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	content, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(content, &tasks); err != nil {
		return nil, err
	}

	return tasks, nil
}

// GetTask returns a single task by its ID
func (repo *FileTaskRepository) GetTask(id int) (Task, error) {
	var task Task

	tasks, err := repo.GetTasks()
	if err != nil {
		return task, err
	}

	for _, t := range tasks {
		if t.ID == id {
			return t, nil
		}
	}

	return task, nil
}

// CreateTask adds a new task to the storage
func (repo *FileTaskRepository) CreateTask(task Task) (Task, error) {
	tasks, err := repo.GetTasks()
	if err != nil {
		return task, err
	}

	task.ID = len(tasks) + 1
	tasks = append(tasks, task)

	if err := repo.SaveTasks(tasks); err != nil {
		return task, err
	}

	return task, nil
}

// UpdateTask updates an existing task in the storage
func (repo *FileTaskRepository) UpdateTask(task Task) (Task, error) {
	tasks, err := repo.GetTasks()
	if err != nil {
		return task, err
	}

	for i, t := range tasks {
		if t.ID == task.ID {
			tasks[i] = task
			break
		}
	}

	if err := repo.SaveTasks(tasks); err != nil {
		return task, err
	}

	return task, nil
}

func (repo *FileTaskRepository) SaveTasks(tasks []Task) error {
	content, err := json.Marshal(tasks)
	if err != nil {
		return err
	}
	return os.WriteFile(repo.FilePath, content, 0644)
}

func (repo *FileTaskRepository) DeleteTask(id int) error {
	tasks, err := repo.GetTasks()
	if err != nil {
		return err
	}
	var newTasks []Task
	for _, t := range tasks {
		if t.ID != id {
			newTasks = append(newTasks, t)
			continue
		}
		repo.SaveTasks(newTasks)
		return nil
	}
	return nil
}

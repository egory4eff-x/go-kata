package service

import (
	"gitlab.com/egory4eff-x/go-kata/module3/clean_architecture/service/model"
	"gitlab.com/egory4eff-x/go-kata/module3/clean_architecture/service/repo"
)

type TodoService interface {
	ListTodos() ([]model.Todo, error)
	CreateTodo(title string) error
	CompleteTodo(todo model.Todo) error
	RemoveTodo(todo model.Todo) error
}

type todoService struct {
	repository repo.TodoRepository
}

func (s *todoService) ListTodos() ([]model.Todo, error) {
	if s.repository == nil {
		return nil, nil
	}
	return s.ListTodos()
}

// создание задачи используя репозиторий и фабрику задач из модуля service и модуля repo
func (s *todoService) CreateTodo(title string) error {
	if s.repository == nil {
		return nil
	}
	return s.CreateTodo(title)
}

func (s *todoService) CompleteTodo(todo model.Todo) error {
	if s.repository == nil {
		return nil
	}
	return s.CompleteTodo(todo)
}

func (s *todoService) RemoveTodo(todo model.Todo) error {
	if s.repository == nil {
		return nil
	}
	return s.RemoveTodo(todo)
}

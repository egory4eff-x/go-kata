package test

import (
	"gitlab.com/egory4eff-x/go-kata/module3/clean_architecture/service/repo"
	"reflect"
	"testing"
)

func TestFileTaskRepository_CreateTask(t *testing.T) {
	type fields struct {
		FilePath string
	}
	type args struct {
		task repo.Task
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    repo.Task
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "first",
			fields: fields{
				FilePath: "test.json",
			},
			args: args{
				task: repo.Task{
					ID:          1,
					Title:       "first",
					Description: "first",
				},
			},
			want: repo.Task{
				ID:          1,
				Title:       "first",
				Description: "first",
			},
			wantErr: false,
		},
		{
			name: "second",
			fields: fields{
				FilePath: "test.json",
			},
			args: args{
				task: repo.Task{
					ID:          2,
					Title:       "second",
					Description: "second",
				},
			},
			want: repo.Task{
				ID:          2,
				Title:       "second",
				Description: "second",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			got, err := repo.CreateTask(tt.args.task)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateTask() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateTask() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFileTaskRepository_DeleteTask(t *testing.T) {
	type fields struct {
		FilePath string
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "first",
			fields: fields{
				FilePath: "test.json",
			},
			args: args{
				id: 1,
			},
			wantErr: false,
		},
		{
			name: "second",
			fields: fields{
				FilePath: "test.json",
			},
			args: args{
				id: 2,
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			if err := repo.DeleteTask(tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("DeleteTask() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestFileTaskRepository_GetTask(t *testing.T) {
	type fields struct {
		FilePath string
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    repo.Task
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "first",
			fields: fields{
				FilePath: "test.json",
			},
			args: args{
				id: 1,
			},
			want: repo.Task{
				ID:          1,
				Title:       "first",
				Description: "first",
			},
			wantErr: false,
		},
		{
			name: "second",
			fields: fields{
				FilePath: "test.json",
			},
			args: args{
				id: 2,
			},
			want: repo.Task{
				ID:          2,
				Title:       "second",
				Description: "second",
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			got, err := repo.GetTask(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetTask() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetTask() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFileTaskRepository_GetTasks(t *testing.T) {
	type fields struct {
		FilePath string
	}
	tests := []struct {
		name    string
		fields  fields
		want    []repo.Task
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "first",
			fields: fields{
				FilePath: "test.json",
			},
			want: []repo.Task{
				{
					ID:          1,
					Title:       "first",
					Description: "first",
				},
				{
					ID:          2,
					Title:       "second",
					Description: "second",
				},
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			got, err := repo.GetTasks()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetTasks() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetTasks() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFileTaskRepository_UpdateTask(t *testing.T) {
	type fields struct {
		FilePath string
	}
	type args struct {
		task repo.Task
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    repo.Task
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "first",
			fields: fields{
				FilePath: "test.json",
			},
			args: args{
				task: repo.Task{
					ID:          1,
					Title:       "first",
					Description: "first",
				},
			},
			want: repo.Task{
				ID:          1,
				Title:       "first",
				Description: "first",
			},
			wantErr: false,
		},
		{
			name: "second",
			fields: fields{
				FilePath: "test.json",
			},
			args: args{
				task: repo.Task{
					ID:          2,
					Title:       "second",
					Description: "second",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			got, err := repo.UpdateTask(tt.args.task)
			if (err != nil) != tt.wantErr {
				t.Errorf("UpdateTask() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UpdateTask() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFileTaskRepository_saveTasks(t *testing.T) {
	type fields struct {
		FilePath string
	}
	type args struct {
		tasks []repo.Task
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "first",
			fields: fields{
				FilePath: "test.json",
			},
			args: args{
				tasks: []repo.Task{
					{
						ID:          1,
						Title:       "first",
						Description: "first",
					},
					{
						ID:          2,
						Title:       "second",
						Description: "second",
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			if err := repo.SaveTasks(tt.args.tasks); (err != nil) != tt.wantErr {
				t.Errorf("saveTasks() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

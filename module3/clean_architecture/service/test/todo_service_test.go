package test

import (
	"gitlab.com/egory4eff-x/go-kata/module3/clean_architecture/service/model"
	"gitlab.com/egory4eff-x/go-kata/module3/clean_architecture/service/repo"
	"gitlab.com/egory4eff-x/go-kata/module3/clean_architecture/service/service"
	_ "gitlab.com/egory4eff-x/go-kata/module3/clean_architecture/service/service"
	"reflect"
	"testing"
)

func Test_todoService_CompleteTodo(t *testing.T) {
	type fields struct {
		repository repo.TodoRepository
	}
	type args struct {
		todo model.Todo
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "Completing a todo",
			fields: fields{
				repository: &repo.MockTodoRepository{
					// Initialize Todo array with sample data
					Todos: []model.Todo{
						{
							ID:          1,
							Title:       "Title",
							Description: "Description",
							Status:      "In progress",
							Tasks:       "Go to the gym",
							Completed:   false,
						},
					},
				},
			},
			args: args{
				todo: model.Todo{
					ID:          1,
					Title:       "Title",
					Description: "Description",
					Status:      "Completed",
					Tasks:       "Go to the gym",
					Completed:   true,
				},
			},
			wantErr: false, // Specify whether you expect an error or not
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service.mockTodoService{
				Repository: tt.fields.repository,
			}
			if err := s.CompleteTodo(tt.args.todo); (err != nil) != tt.wantErr {
				t.Errorf("CompleteTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_todoService_CreateTodo(t *testing.T) {
	type fields struct {
		repository repo.TodoRepository
	}
	type args struct {
		title string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service.todoService{
				repository: tt.fields.repository,
			}
			if err := s.CreateTodo(tt.args.title); (err != nil) != tt.wantErr {
				t.Errorf("CreateTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_todoService_ListTodos(t *testing.T) {
	type fields struct {
		repository repo.TodoRepository
	}
	tests := []struct {
		name    string
		fields  fields
		want    []model.Todo
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service.todoService{
				repository: tt.fields.repository,
			}
			got, err := s.ListTodos()
			if (err != nil) != tt.wantErr {
				t.Errorf("ListTodos() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ListTodos() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_todoService_RemoveTodo(t *testing.T) {
	type fields struct {
		repository repo.TodoRepository
	}
	type args struct {
		todo model.Todo
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service.todoService{
				repository: tt.fields.repository,
			}
			if err := s.RemoveTodo(tt.args.todo); (err != nil) != tt.wantErr {
				t.Errorf("RemoveTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

package main

import "github.com/gin-gonic/gin"

func main() {
	router := gin.Default()

	router.GET("/item", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "Fetch all items",
		})
	})

	router.GET("/item/:id", func(c *gin.Context) {
		id := c.Param("id")
		c.JSON(200, gin.H{
			"message": "Fetch item with id" + id,
		})
	})

	router.Run()
}

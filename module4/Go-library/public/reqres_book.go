package public

import (
	"gitlab.com/egory4eff-x/go-kata/module4/Go-library/internal/model"
)

// swagger:route POST /books/{AuthorId} books bookAddRequest
// Add a new book.
// responses:
// 200: bookAddResponse
// 400: description: Bad request
// 500: description: Internal server error

// swagger:parameters bookAddRequest
//
//nolint:all
type bookAddRequest struct {
	//Author's ID
	//required:true
	//in:path
	AuthorId int `json:"AuthorId"`
	//Book title
	//
	//required:true
	//in:formData
	Title string `json:"title"`
}

// swagger:response bookAddResponse
//
//nolint:all
type bookAddResponse struct {
	//in:body
	Status string
}

// swagger:route GET /books books booksListRequest
// List of books.
// responses:
// 200: booksListResponse
// 400: description: Bad request
// 500: description: Internal server error
//
// swagger:parameters booksListRequest
//
//nolint:all

// swagger:response booksListResponse
//
// swagger:response booksListResponse
//
//nolint:all
type booksListResponse struct {
	// in:body
	Body []model.Book
}

// swagger:route PUT /books/return/{book_id}/users/{user_id} books returnBookRequest
// Put the book back to the library.
// responses:
// 200: returnBookResponse
// 400: description: BadRequest
// 500: description: Internal server error
//
// swagger:parameters returnBookRequest
//
//nolint:all
type returnBookRequest struct {
	// book ID
	//
	// required: true
	// in: path
	BookID int `json:"book_id"`
	// user ID
	//
	// required: true
	// in: path
	UserID int `json:"user_id"`
}

// swagger:response returnBookResponse

//nolint:all
type returnBookResponse struct {
	//in:body
	Status string
}

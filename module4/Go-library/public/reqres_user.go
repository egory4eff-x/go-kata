package public

import "gitlab.com/egory4eff-x/go-kata/module4/Go-library/internal/model"

// swagger:route GET /users users usersListRequest
// List of users.
// responses:
// 200: usersListResponse
//	400: description: Bad request
//	500: description: Internal server error

// swagger:parameters usersListRequest
//
//nolint:all

// swagger:response usersListResponse
//
//nolint:all
type usersListResponse struct {
	//in:body
	Body []model.User
}

// swagger:route PUT /users/takes/{book_id}/users/{user_id} users takeBookByUserRequest
// Get book by user.
// responses:
// 200: takeBookByUserResponse
// 400: description: Bad request
// 500: description: Internal server error
//
// swagger:parameters takeBookByUserRequest
//
//nolint:all
type takeBookByUserRequest struct {
	// book ID
	//
	// required: true
	// in: path
	BookID int `json:"book_id"`
	// user ID
	//
	// required: true
	// in: path
	UserID int `json:"user_id"`
}

// swagger:response takeBookByUserResponse
//
//nolint:all
type takeBookByUserResponse struct {
	//in:body
	Status string
}

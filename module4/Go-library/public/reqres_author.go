package public

import (
	"gitlab.com/egory4eff-x/go-kata/module4/Go-library/internal/model"
)

// swagger:route POST /authors authors authorAddRequest
// Add new author.
// responses:
// 200: authorAddResponse
//  400: description: Bad request
//	500: description: Internal server error

// swagger:parameters authorAddRequest
//
//nolint:all
type authorAddRequest struct {
	//Author's name
	//
	// required:true
	// in:formData
	Name string `json:"name"`
}

// swagger:response authorAddResponse
//
//nolint:all
type authorAddResponse struct {
	//in:body
	Status string
}

// swagger:route GET /authors authors authorsGetListRequest
// List of authors.
// responses:
// 200: authorsGetListResponse
//  400: description: Bad request
//	500: description: Internal server error

// swagger:parameters authorsGetListRequest
//
//nolint:all

// swagger:response authorsGetListResponse
//
//nolint:all
type authorsGetListResponse struct {
	//in:body
	Body []model.Author
}

// swagger:route GET /authors/top authors topMostReadAuthorsRequest
// Top most read authors.
// responses:
// 200: topMostReadAuthorsResponse
//  400: description: Bad request
//	500: description: Internal server error

// swagger:parameters topMostReadAuthorsRequest
//
//nolint:all

// swagger:response topMostReadAuthorsResponse
//
//nolint:all
type topMostReadAuthorsResponse struct {
	//in:body
	Body []model.Author
}

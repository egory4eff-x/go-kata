package handler

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/egory4eff-x/go-kata/module4/Go-library/internal/service"
	"net/http"
	"strconv"
	"time"
)

type BookHandlerInterface interface {
	BooksListRequest() gin.HandlerFunc
	BookAddRequest() gin.HandlerFunc
	ReturnBookRequest() gin.HandlerFunc
}

type BookHandler struct {
	service service.BookServiceInterface
}

func NewBookHandler(service service.BookServiceInterface) *BookHandler {
	return &BookHandler{service: service}
}

func (b *BookHandler) BooksListRequest() gin.HandlerFunc {
	return func(g *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()

		books, err := b.service.SBooksList(ctx)
		if err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}

		g.JSON(http.StatusOK, books)
	}
}

func (b *BookHandler) BookAddRequest() gin.HandlerFunc {
	return func(g *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()

		aId := g.Param("AuthorId")
		authorId, err := strconv.Atoi(aId)
		if err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}

		title := g.PostForm("title")

		err = b.service.SBookAdd(ctx, authorId, title)
		if err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
		}

		g.Writer.WriteHeader(http.StatusOK)
	}
}

func (b *BookHandler) ReturnBookRequest() gin.HandlerFunc {
	return func(g *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()
		bId := g.Param("book_id")
		uId := g.Param("user_id")

		bookId, err := strconv.Atoi(bId)
		if err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}
		userId, err := strconv.Atoi(uId)
		if err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}

		err = b.service.SReturnBook(ctx, bookId, userId)
		if err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}

		g.Writer.WriteHeader(http.StatusOK)
	}
}

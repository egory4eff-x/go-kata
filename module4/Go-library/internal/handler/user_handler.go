package handler

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/egory4eff-x/go-kata/module4/Go-library/internal/service"
	"net/http"
	"strconv"
	"time"
)

type UserHandlerInterface interface {
	ReturnBookRequest() gin.HandlerFunc
	UsersListRequest() gin.HandlerFunc
}

type UserHandler struct {
	service service.UserServiceInterface
}

func NewUserHandler(service service.UserServiceInterface) *UserHandler {
	return &UserHandler{
		service: service,
	}
}

func (u *UserHandler) UsersListRequest() gin.HandlerFunc {
	return func(g *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()

		users, err := u.service.SUsersList(ctx)
		if err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}

		g.JSON(http.StatusOK, users)
	}
}

func (u *UserHandler) GetBookByUserRequest() gin.HandlerFunc {
	return func(g *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()

		uId := g.Param("user_id")
		bId := g.Param("book_id")

		bookId, err := strconv.Atoi(bId)
		if err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}
		userId, err := strconv.Atoi(uId)
		if err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}

		if err = u.service.SGetBookByUser(ctx, bookId, userId); err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}

		g.Writer.WriteHeader(http.StatusOK)
	}
}

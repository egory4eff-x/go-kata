package handler

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/egory4eff-x/go-kata/module4/Go-library/internal/service"
	"net/http"
	"time"
)

type AuthorHandlerInterface interface {
	AuthorsGetListRequest() gin.HandlerFunc
	AuthorsTopListRequest() gin.HandlerFunc
	AuthorAddRequest() gin.HandlerFunc
}

type AuthorHandler struct {
	service service.AuthorServiceInterface
}

func NewAuthorHandler(service service.AuthorServiceInterface) *AuthorHandler {
	return &AuthorHandler{service: service}
}

func (a *AuthorHandler) AuthorsGetListRequest() gin.HandlerFunc {
	return func(g *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()
		authors, err := a.service.SGetListAuthors(ctx)
		if err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}

		g.JSON(http.StatusOK, authors)
	}
}

func (a *AuthorHandler) AuthorsTopListRequest() gin.HandlerFunc {
	return func(g *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()
		authors, err := a.service.SGetTopAuthors(ctx)
		if err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}

		g.JSON(http.StatusOK, authors)
	}
}

func (a *AuthorHandler) AuthorAddRequest() gin.HandlerFunc {
	return func(g *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()

		authorName := g.PostForm("name")

		if err := a.service.SAddAuthor(ctx, authorName); err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}

		g.Writer.WriteHeader(http.StatusOK)
	}
}

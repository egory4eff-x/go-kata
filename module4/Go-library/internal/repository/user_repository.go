package repository

import (
	"context"
	"errors"
	"github.com/jmoiron/sqlx"
	"gitlab.com/egory4eff-x/go-kata/module4/Go-library/internal/model"
	"log"
)

type UserRepositoryInterface interface {
	UsersList(ctx context.Context) ([]model.User, error)
	GetBookByUser(ctx context.Context, bookID int, userID int) error
}

type UserRepository struct {
	db *sqlx.DB
}

func NewUserRepository(db *sqlx.DB) *UserRepository {
	return &UserRepository{
		db: db,
	}
}
func (u *UserRepository) UsersList(ctx context.Context) ([]model.User, error) {
	var users []model.User
	err := u.db.SelectContext(ctx, &users, "SELECT * FROM users")
	if err != nil {
		log.Println(err)
		return nil, err
	}

	for i := range users {
		var books []model.Book
		err := u.db.SelectContext(ctx, &books, `SELECT b.id, b.title, b.author_id, a.name as author_name
		FROM books b 
		LEFT JOIN authors a ON b.author_id=a.id
		WHERE user_id=$1`, users[i].ID)

		if err != nil {
			log.Println(err)
			return nil, err
		}

		for z := range books {
			u := &model.User{ID: users[i].ID, Name: users[i].Name}
			books[z].User = u
			if books[z].AuthorID.Valid {
				a := model.Author{ID: int(books[z].AuthorID.Int64), Name: books[z].AuthorName.String}
				books[z].Author = &a
			}
		}

		users[i].RentedBooks = books
	}
	return users, nil
}

func (u *UserRepository) GetBookByUser(ctx context.Context, bookID int, userID int) error {
	var count int
	err := u.db.GetContext(ctx, &count, "SELECT COUNT(*) FROM books WHERE id=$1 AND user_id IS NULL", bookID)
	if err != nil {
		return err
	}
	if count == 0 {
		return errors.New("book is already rented")
	}

	_, err = u.db.ExecContext(ctx, "UPDATE books SET user_id=$1 WHERE id=$2", userID, bookID)
	if err != nil {
		return err
	}
	return nil
}

package repository

import (
	"context"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/egory4eff-x/go-kata/module4/Go-library/internal/model"
	"log"
)

type BookRepositoryInterface interface {
	BookAdd(ctx context.Context, authorID int, title string) error
	BooksList(ctx context.Context) ([]model.Book, error)
	ReturnBook(ctx context.Context, bookID int, userID int) error
}

type BookRepository struct {
	db *sqlx.DB
}

func NewBookRepository(db *sqlx.DB) *BookRepository {
	return &BookRepository{
		db: db,
	}
}
func (b *BookRepository) BookAdd(ctx context.Context, authorID int, title string) error {
	var count int
	err := b.db.GetContext(ctx, &count, "SELECT count(*) FROM authors WHERE id=$1", authorID)
	if err != nil {
		return err
	}
	if count == 0 {
		return fmt.Errorf("author with ID %d does not exist", authorID)
	}

	_, err = b.db.ExecContext(ctx, "INSERT INTO books (author_id, title) VALUES ($1, $2)", int64(authorID), title)
	if err != nil {
		return err
	}
	return nil
}

func (b *BookRepository) BooksList(ctx context.Context) ([]model.Book, error) {
	var books []model.Book
	err := b.db.SelectContext(ctx, &books,
		`SELECT b.id, b.title, b.author_id, a.name as author_name, b.user_id, u.name as user_name
		FROM books b
		LEFT JOIN authors a ON b.author_id=a.id
		LEFT JOIN users u ON b.user_id=u.id`)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	for z := range books {
		if books[z].UserID.Valid {
			u := model.User{
				ID:   int(books[z].UserID.Int64),
				Name: books[z].UserName.String,
			}
			books[z].User = &u
		}
		if books[z].AuthorID.Valid {
			a := model.Author{
				ID:   int(books[z].AuthorID.Int64),
				Name: books[z].AuthorName.String,
			}
			books[z].Author = &a
		}
	}

	return books, nil
}

func (b *BookRepository) ReturnBook(ctx context.Context, bookID int, userID int) error {
	eff, err := b.db.ExecContext(ctx, "UPDATE books SET user_id=NULL WHERE id=$1 AND user_id=$2", bookID, userID)
	if err != nil {
		log.Println(err)
		return err
	}
	res, _ := eff.RowsAffected() // no books to return
	if res == 0 {
		return errors.New("nothing to return")
	}
	return nil
}

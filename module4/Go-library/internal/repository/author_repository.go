package repository

import (
	"context"
	"errors"
	"github.com/jmoiron/sqlx"
	"gitlab.com/egory4eff-x/go-kata/module4/Go-library/internal/model"
	"log"
	"sort"
)

type AuthorRepositoryInterface interface {
	GetListAuthors(ctx context.Context) ([]model.Author, error)
	AddAuthor(ctx context.Context, name string) error
	GetTopAuthors(ctx context.Context) ([]model.Author, error)
}

type AuthorRepository struct {
	db *sqlx.DB
}

func NewAuthorRepository(db *sqlx.DB) *AuthorRepository {
	return &AuthorRepository{
		db: db,
	}
}

func (a *AuthorRepository) GetListAuthors(ctx context.Context) ([]model.Author, error) {
	var authors []model.Author
	err := a.db.SelectContext(ctx, &authors, "SELECT * FROM authors")
	if err != nil {
		log.Println(err)
		return nil, err
	}
	for i := range authors {
		var books []model.Book
		err := a.db.SelectContext(ctx, &books, `
		SELECT b.id, b.title, b.author_id, a.name as author_name, b.user_id, u.name as user_name
		FROM books b
		LEFT JOIN authors a ON b.author_id = a.id
		LEFT JOIN users u ON b.user_id = u.id
		WHERE b.author_id =$1`, authors[i].ID)
		if err != nil {
			log.Println(err)
			return nil, err
		}

		for j := range books {
			if books[j].UserID.Valid {
				u := model.User{
					ID:   int(books[j].UserID.Int64),
					Name: books[j].UserName.String,
				}
				books[j].User = &u
			}
			if books[j].AuthorID.Valid {
				a := model.Author{
					ID:   int(books[j].AuthorID.Int64),
					Name: books[j].AuthorName.String,
				}
				books[j].Author = &a
			}
		}
		authors[i].Books = books
	}
	return authors, nil
}

func (a *AuthorRepository) GetTopAuthors(ctx context.Context) ([]model.Author, error) {
	var books []model.Book
	var authors []model.Author
	m := make(map[int]int)

	err := a.db.SelectContext(ctx, &books, "SELECT * FROM books WHERE user_id IS NOT NULL")
	if err != nil {
		log.Println(err)
		return nil, err
	}
	for _, book := range books {
		m[int(book.AuthorID.Int64)]++
	}
	err = a.db.SelectContext(ctx, &authors, "SELECT * FROM authors")
	if err != nil {
		log.Println(err)
		return nil, err
	}

	for i, author := range authors {
		if val, ok := m[author.ID]; ok {
			var t model.Top
			t.Readers = val
			authors[i].Top = &t
		}
	}

	sort.Slice(authors, func(i, j int) bool {
		readersI := 0
		if authors[i].Top != nil {
			readersI = authors[i].Top.Readers
		}
		readersJ := 0
		if authors[j].Top != nil {
			readersJ = authors[j].Top.Readers
		}
		return readersI > readersJ
	})

	return authors, nil
}

func (a *AuthorRepository) AddAuthor(ctx context.Context, name string) error {
	_, err := a.db.ExecContext(ctx, "INSERT INTO authors (name) VALUES ($1)", name)
	if err != nil {
		return errors.New("cannot create an author")
	}
	return nil
}

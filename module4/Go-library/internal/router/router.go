package router

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/egory4eff-x/go-kata/module4/Go-library/internal/handler"
)

func Routes(a *handler.AuthorHandler, b *handler.BookHandler, u *handler.UserHandler) *gin.Engine {
	r := gin.Default()
	// Author
	r.GET("/authors", a.AuthorsGetListRequest())
	r.GET("/authors/top", a.AuthorsTopListRequest())
	r.POST("/authors", a.AuthorAddRequest())
	// User
	r.PUT("/users/takes/:book_id/users/:user_id", u.GetBookByUserRequest())
	r.GET("/users", u.UsersListRequest())
	// Book
	r.POST("/books/:AuthorId", b.BookAddRequest())
	r.GET("/books", b.BooksListRequest())
	r.PUT("/books/return/:book_id/users/:user_id", b.ReturnBookRequest())

	return r
}

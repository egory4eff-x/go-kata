package service

import (
	"context"
	"gitlab.com/egory4eff-x/go-kata/module4/Go-library/internal/model"
	"gitlab.com/egory4eff-x/go-kata/module4/Go-library/internal/repository"
)

type BookServiceInterface interface {
	SBookAdd(ctx context.Context, authorID int, title string) error
	SBooksList(ctx context.Context) ([]model.Book, error)
	SReturnBook(ctx context.Context, bookID int, userID int) error
}

type BookService struct {
	repo repository.BookRepositoryInterface
}

func NewBookService(repo repository.BookRepositoryInterface) *BookService {
	return &BookService{
		repo: repo,
	}
}

func (s *BookService) SBookAdd(ctx context.Context, authorID int, title string) error {
	return s.repo.BookAdd(ctx, authorID, title)
}

func (s *BookService) SBooksList(ctx context.Context) ([]model.Book, error) {
	return s.repo.BooksList(ctx)
}

func (s *BookService) SReturnBook(ctx context.Context, bookID int, userID int) error {
	return s.repo.ReturnBook(ctx, bookID, userID)
}

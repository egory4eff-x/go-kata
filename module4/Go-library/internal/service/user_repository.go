package service

import (
	"context"
	"gitlab.com/egory4eff-x/go-kata/module4/Go-library/internal/model"
	"gitlab.com/egory4eff-x/go-kata/module4/Go-library/internal/repository"
)

type UserServiceInterface interface {
	SUsersList(ctx context.Context) ([]model.User, error)
	SGetBookByUser(ctx context.Context, bookID int, userID int) error
}

type UserService struct {
	repo repository.UserRepositoryInterface
}

func NewUserService(repo repository.UserRepositoryInterface) *UserService {
	return &UserService{repo: repo}
}

func (s *UserService) SUsersList(ctx context.Context) ([]model.User, error) {
	return s.repo.UsersList(ctx)
}

func (s *UserService) SGetBookByUser(ctx context.Context, bookID int, userID int) error {
	return s.repo.GetBookByUser(ctx, bookID, userID)
}

package service

import (
	"context"
	"gitlab.com/egory4eff-x/go-kata/module4/Go-library/internal/model"
	"gitlab.com/egory4eff-x/go-kata/module4/Go-library/internal/repository"
)

type AuthorServiceInterface interface {
	SGetListAuthors(ctx context.Context) ([]model.Author, error)
	SAddAuthor(ctx context.Context, name string) error
	SGetTopAuthors(ctx context.Context) ([]model.Author, error)
}

type AuthorService struct {
	repo repository.AuthorRepositoryInterface
}

func NewAuthorService(repo repository.AuthorRepositoryInterface) *AuthorService {
	return &AuthorService{repo: repo}
}

func (s *AuthorService) SGetListAuthors(ctx context.Context) ([]model.Author, error) {
	return s.repo.GetListAuthors(ctx)
}

func (s *AuthorService) SAddAuthor(ctx context.Context, name string) error {
	return s.repo.AddAuthor(ctx, name)
}

func (s *AuthorService) SGetTopAuthors(ctx context.Context) ([]model.Author, error) {
	return s.repo.GetTopAuthors(ctx)
}

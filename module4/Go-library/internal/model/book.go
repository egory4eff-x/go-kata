package model

import "database/sql"

type Book struct {
	ID         int            `json:"id" db:"id"`
	Author     *Author        `json:"author"`
	AuthorID   sql.NullInt64  `db:"authorID"`
	AuthorName sql.NullString `db:"authorName"`
	Title      string         `json:"title" db:"title"`
	User       *User          `json:"user"`
	UserID     sql.NullInt64  `db:"userID"`
	UserName   sql.NullString `db:"userName"`
}

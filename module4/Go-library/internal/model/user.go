package model

type User struct {
	ID          int    `json:"id,omitempty" db:"id"`
	RentedBooks []Book `json:"rentedBooks,omitempty" db:"rentedBooks"`
	Name        string `json:"name,omitempty" db:"name"`
}

package model

type Author struct {
	ID    int    `json:"id,omitempty" db:"id"`
	Name  string `json:"name,omitempty" db:"name"`
	Books []Book `json:"books,omitempty" db:"books"`
	Top   *Top   `json:"top"`
}

type Top struct {
	Readers int `json:"readers"`
}

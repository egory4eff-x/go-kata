package database

import (
	"fmt"
	"github.com/brianvoe/gofakeit/v6"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "12345"
	dbname   = "postgres"
)

type PostgresDB struct {
	Db *sqlx.DB
}

func PostgresConnect() (*PostgresDB, error) {
	connStr := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)

	db, err := sqlx.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	// Создание таблицы users
	createUsersTable := `
CREATE TABLE IF NOT EXISTS users (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL
);

	`
	_, err = db.Exec(createUsersTable)
	if err != nil {
		return nil, err
	}

	// Создание таблицы authors
	createAuthorsTable := `
CREATE TABLE IF NOT EXISTS authors (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

	`
	_, err = db.Exec(createAuthorsTable)
	if err != nil {
		return nil, err
	}

	// Создание таблицы books
	createBooksTable := `
CREATE TABLE IF NOT EXISTS books (
    id SERIAL PRIMARY KEY,
    title TEXT NOT NULL,
    author_id INTEGER REFERENCES authors(id),
    user_id INTEGER REFERENCES users(id)
);
	`
	_, err = db.Exec(createBooksTable)
	if err != nil {
		return nil, err
	}

	fmt.Println("Connected to the database")

	return &PostgresDB{Db: db}, nil
}

func (p *PostgresDB) GenerateData() error {
	// Проверка наличия данных в таблице authors
	var count int
	err := p.Db.QueryRow("SELECT COUNT(*) FROM authors").Scan(&count)
	if err != nil {
		return err
	}

	if count == 0 {
		err = p.generateAuthors(10)
		if err != nil {
			return err
		}
	}

	// Проверка наличия данных в таблице books
	err = p.Db.QueryRow("SELECT COUNT(*) FROM books").Scan(&count)
	if err != nil {
		return err
	}

	if count == 0 {
		err = p.generateBooks(100)
		if err != nil {
			return err
		}
	}

	// Проверка наличия данных в таблице users
	err = p.Db.QueryRow("SELECT COUNT(*) FROM users").Scan(&count)
	if err != nil {
		return err
	}

	if count == 0 {
		err = p.generateUsers(50)
		if err != nil {
			return err
		}
	}

	return nil
}

func (p *PostgresDB) generateAuthors(numAuthors int) error {
	for i := 0; i < numAuthors; i++ {
		authorName := gofakeit.Name()
		_, err := p.Db.Exec("INSERT INTO authors (name) VALUES ($1)", authorName)
		if err != nil {
			return err
		}
	}
	fmt.Printf("Generated %d authors\n", numAuthors)
	return nil
}

func (p *PostgresDB) generateBooks(numBooks int) error {
	var authorIDs []int

	rows, err := p.Db.Query("SELECT id FROM authors")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var authorID int
		err := rows.Scan(&authorID)
		if err != nil {
			return err
		}
		authorIDs = append(authorIDs, authorID)
	}

	for i := 0; i < numBooks; i++ {
		bookTitle := gofakeit.BookTitle()
		authorID := authorIDs[gofakeit.Number(0, len(authorIDs)-1)]
		_, err := p.Db.Exec("INSERT INTO books (title, author_id) VALUES ($1, $2)", bookTitle, authorID)
		if err != nil {
			return err
		}
	}
	fmt.Printf("Generated %d books\n", numBooks)
	return nil
}

func (p *PostgresDB) generateUsers(numUsers int) error {
	for i := 0; i < numUsers; i++ {
		userName := gofakeit.Username()
		_, err := p.Db.Exec("INSERT INTO users (name) VALUES ($1)", userName)
		if err != nil {
			return err
		}
	}
	fmt.Printf("Generated %d users\n", numUsers)
	return nil
}

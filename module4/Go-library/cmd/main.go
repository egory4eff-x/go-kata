package main

import (
	"context"
	"gitlab.com/egory4eff-x/go-kata/module4/Go-library/internal/database"
	"gitlab.com/egory4eff-x/go-kata/module4/Go-library/internal/handler"
	"gitlab.com/egory4eff-x/go-kata/module4/Go-library/internal/repository"
	"gitlab.com/egory4eff-x/go-kata/module4/Go-library/internal/router"
	"gitlab.com/egory4eff-x/go-kata/module4/Go-library/internal/service"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/rs/zerolog/log"
)

func main() {
	// Подключение к базе данных
	db, err := database.PostgresConnect()
	if err != nil {
		log.Fatal().Msgf("Failed to connect to the database: %v", err)
	}

	// Генерация данных
	err = db.GenerateData()
	if err != nil {
		log.Fatal()
	}

	// Создание экземпляра репозитория
	authorRepo := repository.NewAuthorRepository(db.Db) // Используйте db.Db вместо db
	bookRepo := repository.NewBookRepository(db.Db)     // Используйте db.Db вместо db
	userRepo := repository.NewUserRepository(db.Db)     // Используйте db.Db вместо db

	// Создание экземпляра сервиса
	authorService := service.NewAuthorService(authorRepo)
	bookService := service.NewBookService(bookRepo)
	userService := service.NewUserService(userRepo)
	// Создание экземпляра обработчика и передача сервиса
	authorHandler := handler.NewAuthorHandler(authorService)
	bookHandler := handler.NewBookHandler(bookService)
	userHandler := handler.NewUserHandler(userService)
	// Создание маршрутов
	router := router.Routes(authorHandler, bookHandler, userHandler)

	// Запуск сервера
	port := ":8080"
	server := http.Server{
		Addr:    port,
		Handler: router,
	}

	go func() {
		log.Info().Msgf("Server starting at port %v", port)
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.Fatal().Msg("Server stopped")
		}
	}()

	// Ожидание сигнала для завершения сервера
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Info().Msg("Shutdown Server")

	// Установка тайм-аута для завершения сервера
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		log.Fatal().Msgf("Server shutdown failed, %v", err)
	}
	defer func(server *http.Server) {
		err := server.Close()
		if err != nil {
			log.Fatal().Msgf("Failed to close server, %v", err)
		}
	}(&server)

	log.Info().Msg("Server exiting")
}

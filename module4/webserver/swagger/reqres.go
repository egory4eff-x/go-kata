package main

import "gitlab.com/egory4eff-x/go-kata/module4/webserver/swagger/internal/model"

//go:generate swagger generate spec -o /home/luch/work/src/gitlab.com/egory4eff-x/go-kata/module4/webserver/swagger/internal/public/swagger.json --scan-models

// swagger:route POST /pet pet petAddRequest
// Добавление питомца.
// responses:
//   200: petAddResponse

// swagger:parameters petAddRequest
type petAddRequest struct {
	// in:body
	Body model.Pet
}

// swagger:response petAddResponse
type petAddResponse struct {
	// in:body
	Body model.Pet
}

// swagger:route GET /pet/{id} pet petGetByIDRequest
// Получение питомца по id.
// responses:
//   200: petGetByIDResponse

// swagger:parameters petGetByIDRequest
type petGetByIDRequest struct {
	// ID of an item
	//
	// In: path
	ID string `json:"id"`
}

// swagger:response petGetByIDResponse
type petGetByIDResponse struct {
	// in:body
	Body model.Pet
}

// swagger:route POST /user user userAddRequest
// Добавление юзера.
// responses:
//   200: userAddResponse

// swagger:parameters userAddRequest
type userAddRequest struct {
	Body model.User
}

// swagger:response userAddResponse
type userAddResponse struct {
	Body model.User
}

// swagger:route GET /user/{id} user userGetByIDRequest
// Получение юзера по id.
// responses:
//   200: userGetByIDResponse

// swagger:parameters userGetByIDRequest
type userGetByIDRequest struct {
	ID string `json:"id"`
}

// swagger:response userGetByIDResponse
type userGetByIDResponse struct {
	Body model.User
}

// swagger:route POST /store store storeAddRequest
// Добавление магазина.
// responses:
//   200: storeAddResponse

// swagger:parameters storeAddRequest
type storeAddRequest struct {
	Body model.Store
}

// swagger:parameters storeAddResponse
type storeAddResponse struct {
	Body model.Store
}

// swagger:route GET /store/{id} store storeGetByIDRequest
// Получение магазина по id.
// responses:
//   200: storeGetByIDResponse

// swagger:parameters storeGetByIDRequest
type storeGetByIDRequest struct {
	ID string `json:"id"`
}

// swagger:parameters storeGetByResponse
type storeGetByResponse struct {
	Body model.Store
}

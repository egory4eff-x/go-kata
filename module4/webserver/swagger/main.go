package main

import (
	"context"
	"fmt"
	"gitlab.com/egory4eff-x/go-kata/module4/webserver/swagger/internal/controller"
	"html/template"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

const (
	swaggerTemplate = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-standalone-preset.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-standalone-preset.js"></script> -->
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-bundle.js"></script> -->
    <link rel="stylesheet" href="//unpkg.com/swagger-ui-dist@3/swagger-ui.css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui.css" /> -->
  <style>
    body {
      margin: 0;
    }
  </style>
    <title>Swagger</title>
</head>
<body>
    <div id="swagger-ui"></div>
    <script>
        window.onload = function() {
          SwaggerUIBundle({
            url: "/public/swagger.json?{{.Time}}",
            dom_id: '#swagger-ui',
            presets: [
              SwaggerUIBundle.presets.apis,
              SwaggerUIStandalonePreset
            ],
            layout: "StandaloneLayout"
          })
        }
    </script>
</body>
</html>
`
)

func swaggerUI(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	tmpl, err := template.New("swagger").Parse(swaggerTemplate)
	if err != nil {
		return
	}
	err = tmpl.Execute(w, struct {
		Time int64
	}{
		Time: time.Now().Unix(),
	})
	if err != nil {
		return
	}
}

func main() {
	port := ":8080"

	r := chi.NewRouter()
	r.Use(middleware.Logger)

	petController := controller.NewPetController()

	r.Get("/pet", petController.PetGetList)
	r.Post("/pet", petController.PetCreate)
	r.Put("/pet", petController.PetUpdatePut)
	r.Get("/pet/findByStatus", petController.PetGetByStatus)
	r.Post("/pet/{petID}/uploadImage", petController.PetUploadImage)
	r.Post("/pet/{petID}", petController.PetUpdatePost)
	r.Get("/pet/{petID}", petController.PetGetByID)
	r.Delete("/pet/{petID}", petController.PetDelete)

	//пользователь
	userController := controller.NewUserController()

	r.Get("/user", userController.UserGetList)
	r.Post("/user", userController.UserCreate)
	r.Put("/user", userController.UserUpdatePut)
	r.Post("/user/{userID}", userController.UserGetByID)
	r.Post("/pet/{userID}", userController.UserUpdatePost)
	r.Delete("/user/{userID}", userController.UserDelete)

	//магазин

	storeController := controller.NewStoreController()
	r.Get("/store", storeController.StoreGetList)
	r.Post("/store", storeController.StoreCreate)
	r.Post("/store/{storeID}", storeController.StoreGetByID)
	r.Delete("/store/{storeID}", storeController.StoreDelete)

	//SwaggerUI
	r.Get("/swagger", swaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./module4/webserver/swagger/public"))).ServeHTTP(w, r)
	})

	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}

	// Запуск веб-сервера в отдельном горутине
	go func() {
		log.Println(fmt.Sprintf("service started on port %s ", port))
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}

package controller

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"gitlab.com/egory4eff-x/go-kata/module4/webserver/swagger/internal/model"
	"gitlab.com/egory4eff-x/go-kata/module4/webserver/swagger/internal/repository"
	"net/http"
	"strconv"
)

type UserController struct {
	storage repository.UserStorage
}

func NewUserController() *UserController {
	return &UserController{
		storage: repository.NewUserStorage(),
	}
}

func (u *UserController) UserCreate(w http.ResponseWriter, r *http.Request) {
	var user model.User
	err := json.NewDecoder(r.Body).Decode(&user) // считываем приходящий json из *http.Request в структуру Pet

	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	user = u.storage.Create(user) // создаем запись в нашем storage

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(user)                            // записываем результат Pet json в http.ResponseWriter

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (u *UserController) UserGetByID(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		user      model.User
		err       error
		userIDRaw string
		userID    int
	)

	userIDRaw = chi.URLParam(r, "petID") // получаем petID из chi handler

	userID, err = strconv.Atoi(userIDRaw) // конвертируем в int
	if err != nil {                       // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	user, err = u.storage.GetByID(userID) // пытаемся получить Pet по id
	if err != nil {                       // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (u *UserController) UserGetList(w http.ResponseWriter, r *http.Request) {
	users := u.storage.GetList()

	w.Header().Set("Content-Type", "application/json:charset=utf-8")
	err := json.NewEncoder(w).Encode(users)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (u *UserController) UserUpdatePut(w http.ResponseWriter, r *http.Request) {
	var user model.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	user, err = u.storage.Update(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (u *UserController) UserUpdatePost(w http.ResponseWriter, r *http.Request) {
	var user model.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	user, err = u.storage.Update(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (u *UserController) UserDelete(w http.ResponseWriter, r *http.Request) {
	userIDRaw := chi.URLParam(r, "userID")
	petID, err := strconv.Atoi(userIDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = u.storage.Delete(petID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

package controller

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"gitlab.com/egory4eff-x/go-kata/module4/webserver/swagger/internal/model"
	"gitlab.com/egory4eff-x/go-kata/module4/webserver/swagger/internal/repository"
	"net/http"
	"strconv"
)

type StoreController struct {
	storage repository.StoreStorage
}

func NewStoreController() *StoreController {
	return &StoreController{
		storage: repository.NewStoreStorage(),
	}
}

func (s *StoreController) StoreCreate(w http.ResponseWriter, r *http.Request) {
	var store model.Store
	err := json.NewDecoder(r.Body).Decode(&store)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	store = s.storage.Create(store)

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(store)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *StoreController) StoreDelete(w http.ResponseWriter, r *http.Request) {
	storeIDRaw := chi.URLParam(r, "petID")
	storeID, err := strconv.Atoi(storeIDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = s.storage.Delete(storeID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (s *StoreController) StoreGetByID(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		store      model.Store
		err        error
		storeIDRaw string
		storeID    int
	)

	storeIDRaw = chi.URLParam(r, "storeID")

	storeID, err = strconv.Atoi(storeIDRaw) // конвертируем в int
	if err != nil {                         // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	store, err = s.storage.GetByID(storeID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(store)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (s *StoreController) StoreGetList(w http.ResponseWriter, r *http.Request) {
	pets := s.storage.GetList()

	w.Header().Set("Content-Type", "application/json:charset=utf-8")
	err := json.NewEncoder(w).Encode(pets)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

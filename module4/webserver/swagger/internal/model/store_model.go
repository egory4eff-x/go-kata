package model

import (
	"encoding/json"
)

func Unmarshal(data []byte) (Store, error) {
	var s Store
	err := json.Unmarshal(data, &s)
	return s, err
}

func (s *Store) Marshal() ([]byte, error) {
	return json.Marshal(s)
}

type Store struct {
	ID       int    `json:"id"`
	PetID    int    `json:"petId"`
	Quantity int    `json:"quantity"`
	ShipDate string `json:"shipDate"`
	Status   string `json:"status"`
	Complete bool   `json:"complete"`
}

package model

import "encoding/json"

func Unmarshall(data []byte) (User, error) {
	var u User

	err := json.Unmarshal(data, &u)
	return u, err
}

func (u *User) Marshall() ([]byte, error) {
	return json.Marshal(u)
}

type User struct {
	ID         int    `json:"id"`
	FirstName  string `json:"firstName"`
	LastName   string `json:"lastName"`
	Email      string `json:"email"`
	Phone      string `json:"phone"`
	UserStatus int    `json:"userStatus"`
}

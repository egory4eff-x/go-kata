package repository

import (
	"fmt"
	"gitlab.com/egory4eff-x/go-kata/module4/webserver/swagger/internal/model"
	"sync"
)

type PetStorager interface {
	Create(pet model.Pet) model.Pet                 //создание
	Update(pet model.Pet) (model.Pet, error)        //обновление
	Delete(petID int) error                         //удаление
	GetByID(petID int) (model.Pet, error)           //получение по ID
	GetList() []model.Pet                           //получение по листу
	GetByStatus(status string) ([]model.Pet, error) //получение статуса
}

type PetStorage struct {
	data               []*model.Pet
	primaryKeyIDx      map[int]*model.Pet
	autoIncrementCount int
	sync.Mutex
}

func NewPetStorage() PetStorage {
	return PetStorage{
		data:               make([]*model.Pet, 0, 13),
		primaryKeyIDx:      make(map[int]*model.Pet, 13),
		autoIncrementCount: 1,
	}
}

func (p PetStorage) Create(pet model.Pet) model.Pet {
	p.Lock()
	defer p.Unlock()
	pet.ID = p.autoIncrementCount
	p.primaryKeyIDx[pet.ID] = &pet
	p.data = append(p.data, &pet)

	return pet
}

func (p PetStorage) Update(pet model.Pet) (model.Pet, error) {
	p.Lock()
	defer p.Unlock()
	if _, ok := p.primaryKeyIDx[pet.ID]; !ok {
		return model.Pet{}, fmt.Errorf("not found")
	}

	p.primaryKeyIDx[pet.ID] = &pet

	for i, storedPet := range p.data {
		if storedPet.ID == pet.ID {
			p.data[i] = &pet
			break
		}
	}

	return pet, nil

}

func (p PetStorage) Delete(petID int) error {
	p.Lock()
	defer p.Unlock()

	if _, ok := p.primaryKeyIDx[petID]; !ok {
		return fmt.Errorf("not found")
	}

	delete(p.primaryKeyIDx, petID)

	for i, storedPet := range p.data {
		if storedPet.ID == petID {
			p.data = append(p.data[:i], p.data[i+1:]...)
			break
		}
	}

	return nil
}

func (p PetStorage) GetByID(petID int) (model.Pet, error) {
	if v, ok := p.primaryKeyIDx[petID]; ok {
		return *v, nil
	}

	return model.Pet{}, fmt.Errorf("not found")
}

func (p PetStorage) GetList() []*model.Pet {
	return p.data
}

func (p *PetStorage) GetByStatus(status string) ([]model.Pet, error) {
	var result []model.Pet
	for _, pet := range p.data {
		if pet.Status == status {
			result = append(result, *pet)
		}
	}

	if len(result) > 0 {
		return result, nil
	} else {
		return nil, fmt.Errorf("no pets found with status %s", status)
	}
}

package repository

import (
	"gitlab.com/egory4eff-x/go-kata/module4/webserver/swagger/internal/model"
	"reflect"
	"sync"
	"testing"
)

func TestNewUserStorage(t *testing.T) {
	tests := []struct {
		name string
		want UserStorage
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewUserStorage(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewUserStorage() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserStorage_Create(t *testing.T) {
	type fields struct {
		data               []*model.User
		primaryKeyIDx      map[int]*model.User
		autoIncrementCount int
		Mutex              sync.Mutex
	}
	type args struct {
		user model.User
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   model.User
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UserStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			if got := u.Create(tt.args.user); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserStorage_Delete(t *testing.T) {
	type fields struct {
		data               []*model.User
		primaryKeyIDx      map[int]*model.User
		autoIncrementCount int
		Mutex              sync.Mutex
	}
	type args struct {
		userID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UserStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			if err := u.Delete(tt.args.userID); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUserStorage_GetByID(t *testing.T) {
	type fields struct {
		data               []*model.User
		primaryKeyIDx      map[int]*model.User
		autoIncrementCount int
		Mutex              sync.Mutex
	}
	type args struct {
		userID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.User
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UserStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			got, err := u.GetByID(tt.args.userID)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByID() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserStorage_GetList(t *testing.T) {
	type fields struct {
		data               []*model.User
		primaryKeyIDx      map[int]*model.User
		autoIncrementCount int
		Mutex              sync.Mutex
	}
	tests := []struct {
		name   string
		fields fields
		want   []*model.User
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UserStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			if got := u.GetList(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetList() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserStorage_Update(t *testing.T) {
	type fields struct {
		data               []*model.User
		primaryKeyIDx      map[int]*model.User
		autoIncrementCount int
		Mutex              sync.Mutex
	}
	type args struct {
		user model.User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.User
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UserStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			got, err := u.Update(tt.args.user)
			if (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Update() got = %v, want %v", got, tt.want)
			}
		})
	}
}

package repository

import (
	"gitlab.com/egory4eff-x/go-kata/module4/webserver/swagger/internal/model"
	"reflect"
	"sync"
	"testing"
)

func TestPetStorage_Create(t *testing.T) {
	type fields struct {
		data               []*model.Pet
		primaryKeyIDx      map[int64]*model.Pet
		autoIncrementCount int64
		Mutex              sync.Mutex
	}
	type args struct {
		pet model.Pet
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   model.Pet
	}{
		{
			name: "first test",
			args: args{
				pet: model.Pet{
					ID: 0,
					Category: model.Category{
						ID:   0,
						Name: "test category",
					},
					Name: "Alma",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags:   []model.Category{},
					Status: "active",
				},
			},
			want: model.Pet{
				ID: 0,
				Category: model.Category{
					ID:   0,
					Name: "test category",
				},
				Name: "Alma",
				PhotoUrls: []string{
					"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
					"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
					"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
				},
				Tags:   []model.Category{},
				Status: "active",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewPetStorage()
			var got model.Pet
			var err error
			got = p.Create(tt.args.pet)
			tt.want.ID = got.ID // fix autoincrement value
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}
			if got, err = p.GetByID(got.ID); err != nil || !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v, err %s", got, tt.want, err)
			}
		})
	}
}

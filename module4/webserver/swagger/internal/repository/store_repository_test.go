package repository

import (
	"gitlab.com/egory4eff-x/go-kata/module4/webserver/swagger/internal/model"
	"reflect"
	"sync"
	"testing"
)

func TestNewStoreStorage(t *testing.T) {
	tests := []struct {
		name string
		want StoreStorage
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewStoreStorage(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewStoreStorage() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStoreStorage_Create(t *testing.T) {
	type fields struct {
		data               []*model.Store
		primaryKeyIDx      map[int]*model.Store
		autoIncrementCount int
		Mutex              sync.Mutex
	}
	type args struct {
		store model.Store
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   model.Store
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := StoreStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			if got := s.Create(tt.args.store); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStoreStorage_Delete(t *testing.T) {
	type fields struct {
		data               []*model.Store
		primaryKeyIDx      map[int]*model.Store
		autoIncrementCount int
		Mutex              sync.Mutex
	}
	type args struct {
		storeID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := StoreStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			if err := s.Delete(tt.args.storeID); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestStoreStorage_GetByID(t *testing.T) {
	type fields struct {
		data               []*model.Store
		primaryKeyIDx      map[int]*model.Store
		autoIncrementCount int
		Mutex              sync.Mutex
	}
	type args struct {
		storeID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.Store
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := StoreStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			got, err := s.GetByID(tt.args.storeID)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByID() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStoreStorage_GetList(t *testing.T) {
	type fields struct {
		data               []*model.Store
		primaryKeyIDx      map[int]*model.Store
		autoIncrementCount int
		Mutex              sync.Mutex
	}
	tests := []struct {
		name   string
		fields fields
		want   []*model.Store
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := StoreStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			if got := s.GetList(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetList() = %v, want %v", got, tt.want)
			}
		})
	}
}

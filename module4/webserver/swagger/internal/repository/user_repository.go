package repository

import (
	"fmt"
	"gitlab.com/egory4eff-x/go-kata/module4/webserver/swagger/internal/model"
	"sync"
)

type UserStorager interface {
	Create(user model.User) model.User
	GetByID(userID int) (model.User, error)
	GetList() []model.User
	Update(user model.User) (model.User, error)
	Delete(userID int) error
}

type UserStorage struct {
	data               []*model.User
	primaryKeyIDx      map[int]*model.User
	autoIncrementCount int
	sync.Mutex
}

func NewUserStorage() UserStorage {
	return UserStorage{
		data:               make([]*model.User, 0, 13),
		primaryKeyIDx:      make(map[int]*model.User, 13),
		autoIncrementCount: 1,
	}
}

func (u UserStorage) Create(user model.User) model.User {
	u.Lock()
	defer u.Unlock()

	user.ID = u.autoIncrementCount
	u.primaryKeyIDx[user.ID] = &user
	u.data = append(u.data, &user)

	return user
}

func (u UserStorage) GetByID(userID int) (model.User, error) {
	if v, ok := u.primaryKeyIDx[userID]; !ok {
		return *v, nil
	}
	return model.User{}, fmt.Errorf("not found")
}

func (u UserStorage) GetList() []*model.User {
	return u.data
}

func (u UserStorage) Update(user model.User) (model.User, error) {
	u.Lock()
	defer u.Unlock()

	if _, ok := u.primaryKeyIDx[user.ID]; !ok {
		return model.User{}, fmt.Errorf("not found")
	}

	u.primaryKeyIDx[user.ID] = &user

	for i, use := range u.data {
		if use.ID == user.ID {
			u.data = append(u.data[:i], u.data[i+1:]...)
			break
		}
	}
	return user, nil
}

func (u UserStorage) Delete(userID int) error {
	u.Lock()
	defer u.Unlock()

	if _, ok := u.primaryKeyIDx[userID]; !ok {
		return fmt.Errorf("not found")
	}

	for i, user := range u.data {
		if user.ID == userID {
			u.data = append(u.data[:i], u.data[i+1:]...)
			break
		}
	}

	return nil
}

package repository

import (
	"fmt"
	"gitlab.com/egory4eff-x/go-kata/module4/webserver/swagger/internal/model"
	"sync"
)

type StoreStorager interface {
	Create(store model.Store) model.Store
	Delete(storeID int) error
	GetByID(storeID model.Store) (model.Store, error)
	GetList() []model.Store
}

type StoreStorage struct {
	data               []*model.Store
	primaryKeyIDx      map[int]*model.Store
	autoIncrementCount int
	sync.Mutex
}

func NewStoreStorage() StoreStorage {
	return StoreStorage{
		data:               make([]*model.Store, 0, 13),
		primaryKeyIDx:      make(map[int]*model.Store, 13),
		autoIncrementCount: 1,
	}
}

func (s StoreStorage) Create(store model.Store) model.Store {
	s.Lock()
	defer s.Unlock()
	store.ID = s.autoIncrementCount
	s.primaryKeyIDx[store.ID] = &store
	s.data = append(s.data, &store)

	return store
}

func (s StoreStorage) Delete(storeID int) error {
	s.Lock()
	defer s.Unlock()

	if _, ok := s.primaryKeyIDx[storeID]; !ok {
		return fmt.Errorf("not found")
	}
	delete(s.primaryKeyIDx, storeID)

	for i, store := range s.data {
		if store.ID == storeID {
			s.data = append(s.data[:i], s.data[i+1:]...)
			break
		}
	}
	return nil

}

func (s StoreStorage) GetByID(storeID int) (model.Store, error) {
	if v, ok := s.primaryKeyIDx[storeID]; ok {
		return *v, nil
	}

	return model.Store{}, fmt.Errorf("not found")
}

func (s StoreStorage) GetList() []*model.Store {
	return s.data
}

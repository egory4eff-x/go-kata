package service

import (
	"errors"
)

type User struct {
	ID   string `json:"id"`
	Name string `json:"name"`
	Age  int    `json:"age"`
}

type UserRepository interface {
	GetAllUsers() []User
	GetUserByID(id string) (*User, error)
	CreateUser(user *User) error
}

type UserService struct {
	userRepository UserRepository
}

func NewUserService(userRepository UserRepository) *UserService {
	return &UserService{
		userRepository: userRepository,
	}
}

func (s *UserService) GetAllUsers() []User {
	return s.userRepository.GetAllUsers()
}

func (s *UserService) GetUserByID(id string) (*User, error) {
	return s.userRepository.GetUserByID(id)
}

func (s *UserService) CreateUser(user *User) error {
	if user.ID == "" {
		return errors.New("User ID is required")
	}
	return s.userRepository.CreateUser(user)
}

package repository

import (
	"errors"
	"gitlab.com/egory4eff-x/go-kata/module4/webserver/http/homework/internal/service"
)

type User struct {
	ID   string `json:"id"`
	Name string `json:"name"`
	Age  int    `json:"age"`
}

type UserRepository interface {
	GetAllUsers() []service.User
	GetUserByID(id string) (*service.User, error)
	CreateUser(user *service.User) error
}

type InMemoryUserRepository struct {
	users []User
}

func NewInMemoryUserRepository() *InMemoryUserRepository {
	return &InMemoryUserRepository{
		users: make([]User, 0),
	}
}

func (r *InMemoryUserRepository) GetAllUsers() []service.User {
	users := make([]service.User, len(r.users))
	for i, user := range r.users {
		users[i] = service.User{
			ID:   user.ID,
			Name: user.Name,
			Age:  user.Age,
		}
	}
	return users
}

func (r *InMemoryUserRepository) GetUserByID(id string) (*service.User, error) {
	for _, user := range r.users {
		if user.ID == id {
			return &service.User{
				ID:   user.ID,
				Name: user.Name,
				Age:  user.Age,
			}, nil
		}
	}
	return nil, errors.New("User not found")
}

func (r *InMemoryUserRepository) CreateUser(user *service.User) error {
	r.users = append(r.users, User{
		ID:   user.ID,
		Name: user.Name,
		Age:  user.Age,
	})
	return nil
}

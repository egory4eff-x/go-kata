package main

import (
	"fmt"
	"net/http"
)

func hudler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, World!")
}

func main() {
	http.HandleFunc("/", hudler)
	http.ListenAndServe(":8080", nil)
}

package storage

import (
	"context"
	"encoding/json"
	"github.com/go-redis/redis"
	"gitlab.com/ptflp/geotask/module/courier/models"
)

//go:generate go run github.com/vektra/mockery/v2@v2.28.2 --name=CourierStorager
type CourierStorager interface {
	Save(ctx context.Context, courier models.Courier) error // сохранить курьера по ключу courier
	GetOne(ctx context.Context) (*models.Courier, error)    // получить курьера по ключу courier
}

type CourierStorage struct {
	storage *redis.Client
}

func NewCourierStorage(storage *redis.Client) CourierStorager {
	return &CourierStorage{storage: storage}
}

func (c *CourierStorage) Save(ctx context.Context, courier models.Courier) error {
	var err error
	var data []byte

	data, err = json.Marshal(courier)
	if err != nil {
		return err
	}

	return c.storage.Set("courier", data, 0).Err()
}

func (c *CourierStorage) GetOne(ctx context.Context) (*models.Courier, error) {
	var (
		data    []byte
		err     error
		courier models.Courier
	)
	data, err = c.storage.Get("courier").Bytes()
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(data, &courier)
	if err != nil {
		return nil, err
	}

	return &courier, nil
}

package docs

// добавить документацию для роута /api/status

import models "gitlab.com/ptflp/geotask/module/courierfacade/models"

func init() {
	_ = courierGetStatusRequest{}
	_ = courierGetStatusResponse{}
}

// добавить документацию для роута /api/status

// swagger:route GET /api/status courier courierGetStatusRequest
// Статус курьера
// responses:
// 200: courierGetStatusResponse

// swagger:parameters courierGetStatusRequest
type courierGetStatusRequest struct {
}

// swagger:response courierGetStatusResponse
type courierGetStatusResponse struct {
	Body models.CourierStatus
}

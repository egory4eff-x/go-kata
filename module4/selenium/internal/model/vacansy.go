package model

type Vacancy struct {
	Context     string `json:"@context"`
	Type        string `json:"@type"`
	DatePosted  string `json:"datePosted"`
	Title       string `json:"title"`
	Description string `json:"description"`

	Identifier struct {
		IdentifierType string `json:"@type"`
		IdentifierName string `json:"name"`
		Value          string `json:"value"`
	}

	HiringOrganization struct {
		Type   string `json:"@type"`
		Name   string `json:"name"`
		SameAs string `json:"sameAs"`
	}

	JobLocation struct {
		PlaceType string `json:"@type"`
		Address   struct {
			AdressType      string `json:"@type"`
			StreetAddress   string `json:"streetAddress"`
			AddressLocality string `json:"addressLocality"`
			AddressCountry  struct {
				CountryType string `json:"@type"`
				CountryName string `json:"name"`
			}
		}
	}
	JobLocationType string `json:"jobLocationType"`
	EmploymentType  string `json:"employmentType"`
}

module gitlab.com/egory4eff-x/go-kata/module4/rewiew

go 1.20

require (
	github.com/go-chi/chi v1.5.4
	github.com/jmoiron/sqlx v1.3.5
	github.com/lib/pq v1.10.9
)

require (
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/rs/zerolog v1.29.1 // indirect
	golang.org/x/sys v0.0.0-20210927094055-39ccf1dd6fa6 // indirect
)

package main

import (
	"context"
	handler "gitlab.com/egory4eff-x/go-kata/module4/rewiew/internal/handler"
	"gitlab.com/egory4eff-x/go-kata/module4/rewiew/internal/repository"
	"gitlab.com/egory4eff-x/go-kata/module4/rewiew/internal/service"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/rs/zerolog/log"
)

func main() {
	// Создаем экземпляр репозитория и сервиса
	repo, _ := repository.NewPostgresDB()
	service := service.NewService(repo)

	// Создаем экземпляр обработчика и передаем сервис
	handler := handler.NewHandler(service)

	// Создаем маршруты
	router := handler.Router()

	// Запускаем сервер
	port := ":8080"
	server := http.Server{
		Addr:    port,
		Handler: router,
	}

	go func() {
		log.Info().Msgf("Server starting at port %v", port)
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.Fatal().Msg("Server stopped")
		}
	}()

	// Ожидание сигнала для завершения сервера
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Info().Msg("Shutdown Server")

	// Устанавливаем таймаут завершения сервера
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		log.Fatal().Msgf("Server shutdown failed, %v", err)
	}
	defer func(server *http.Server) {
		err := server.Close()
		if err != nil {
			log.Fatal().Msgf("Failed to close service, %v", err)
		}
	}(&server)

	log.Info().Msg("Server exiting")
}

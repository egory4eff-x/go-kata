package repository

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/egory4eff-x/go-kata/module4/rewiew/internal/model"
)

type UserRepository interface {
	GetAll() ([]model.User, error)
	Create(user model.User) (model.User, error)
	Update(id int, updatedUser model.User) (model.User, error)
	Delete(id int) error
}

type PostgresDB struct {
	db *sqlx.DB
}

func NewPostgresDB() (*PostgresDB, error) {
	//Вводимые параметры к базе данных
	connStr := "user=postgres password=12345 dbname=postgres host=localhost port=5432 sslmode=disable"

	//Открытие соединения с базой данных
	db, err := sqlx.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}

	//Проверка соединения с базой данных
	err = db.Ping()
	if err != nil {
		return nil, err
	}

	//Создание таблицы, если она не существует

	_, err = db.Exec(`
	CREATE TABLE IF NOT EXISTS Users ( 
		id SERIAL PRIMARY KEY, 
		first_name VARCHAR(50), 
		last_name VARCHAR(50), 
		age INT, 
		email VARCHAR(100), 
		phone VARCHAR(20), 
		address VARCHAR(100), 
		city VARCHAR(50), 
		country VARCHAR(50)
	);
`)
	if err != nil {
		return nil, err
	}

	_, err = db.Exec(`
	INSERT INTO Users (first_name, last_name, age, email, phone, address, city, country)
	VALUES
		('John', 'Doe', 25, 'johndoe@example.com', '123-456-7890', '123 Main St', 'New York', 'USA'),
		('Jane', 'Smith', 32, 'janesmith@example.com', '987-654-3210', '456 Elm St', 'Los Angeles', 'USA'),
		('Mike', 'Johnson', 41, 'mikejohnson@example.com', '555-123-4567', '789 Oak Ave', 'Chicago', 'USA'),
		('Emily', 'Brown', 29, 'emilybrown@example.com', '111-222-3333', '321 Pine Rd', 'London', 'UK'),
		('Alex', 'Lee', 37, 'alexlee@example.com', '444-555-6666', '567 Maple Dr', 'Toronto', 'Canada');
`)
	if err != nil {
		return nil, err
	}

	return &PostgresDB{db: db}, nil
}

//Реализация CRUD методов

// Получение всех пользователей
func (p *PostgresDB) GetAll() ([]model.User, error) {
	var users []model.User
	err := p.db.Select(&users, "SELECT * FROM users")
	if err != nil {
		return nil, err
	}
	return users, nil
}

// Создание нового пользователя
func (p *PostgresDB) Create(user model.User) (model.User, error) {
	var createUser model.User
	err := p.db.QueryRowx("INSERT INTO users (first_name, last_name, age, email, phone, address, city, country) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING *",
		user.First_name, user.Last_name, user.Age, user.Email, user.Phone, user.Address, user.City, user.Country).StructScan(&createUser)
	if err != nil {
		return model.User{}, err
	}
	return createUser, nil
}

// Обновление записи в таблице
func (p *PostgresDB) Update(id int, updateUser model.User) (model.User, error) {
	var user model.User
	err := p.db.Get(&user, `
				UPDATE users SET 
                 first_name = $1, 
                 last_name = $2, 
                 age = $3, 
                 email = $4, 
                 phone = $5, 
                 address = $6, 
                 city = $7, 
                 country = $8 
				WHERE id = $9 RETURNING *`,
		updateUser.First_name, updateUser.Last_name, updateUser.Age, updateUser.Email, updateUser.Phone, updateUser.Address, updateUser.City, updateUser.Country, id)
	if err != nil {
		return model.User{}, err
	}
	return user, nil
}

// Удаление пользователя из таблицы
func (p *PostgresDB) Delete(id int) error {
	_, err := p.db.Exec("DELETE FROM users WHERE id = $1", id)
	if err != nil {
		return err
	}
	return nil
}

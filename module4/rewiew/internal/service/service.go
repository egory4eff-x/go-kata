package service

import (
	"gitlab.com/egory4eff-x/go-kata/module4/rewiew/internal/model"
	"gitlab.com/egory4eff-x/go-kata/module4/rewiew/internal/repository"
)

type UserService interface {
	SGetAll() ([]model.User, error)
	SCreate(user model.User) (model.User, error)
	SUpdate(id int, updateUser model.User) (model.User, error)
	SDelete(id int) error
}

type Service struct {
	repo repository.UserRepository
}

func NewService(repo repository.UserRepository) *Service {
	return &Service{repo: repo}
}

func (s *Service) SGetAll() ([]model.User, error) {
	return s.repo.GetAll()
}

func (s *Service) SCreate(user model.User) (model.User, error) {
	return s.repo.Create(user)
}

func (s *Service) SUpdate(id int, updateUser model.User) (model.User, error) {
	return s.repo.Update(id, updateUser)
}

func (s *Service) SDelete(id int) error {
	return s.repo.Delete(id)
}

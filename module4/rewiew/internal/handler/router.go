package handlers

import "github.com/go-chi/chi"

func (h *Handler) Router() *chi.Mux {
	r := chi.NewRouter()
	r.Get("/user", h.GetAllUsers)
	r.Post("/user", h.CreatUser)
	r.Put("/user/{id}", h.UpdateUser)
	r.Delete("/users/{id}", h.DeleteUser)

	return r
}

package model

type User struct {
	First_name string `json:"first_name"`
	Last_name  string `json:"last_name"`
	Age        int    `json:"age"`
	Email      string `json:"email"`
	Phone      string `json:"phone"`
	ID         int    `json:"id"`
	Address    string `json:"address"`
	City       string `json:"city"`
	Country    string `json:"country"`
}

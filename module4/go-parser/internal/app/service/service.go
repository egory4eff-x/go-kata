package service

import "gitlab.com/egory4eff-x/go-kata/module4/go-parser/internal/app/model"

type MyRepository interface {
	AddVacancy(vacancy model.Vacancy) error
	GetByID(id int) (model.Vacancy, error)
	GetList() ([]model.Vacancy, error)
	Delete(id int) error
}

type MyService interface {
	NewVacancy(vacancy model.Vacancy) error
	GetVacancyByID(id int) (model.Vacancy, error)
	GetVacancyList() ([]model.Vacancy, error)
	DeleteVacancy(id int) error
}

type Service struct {
	repo MyRepository
}

func NewService(repo MyRepository) *Service {
	return &Service{repo: repo}
}

func (s *Service) NewVacancy(vacancy model.Vacancy) error {
	return s.repo.AddVacancy(vacancy)
}

func (s *Service) GetVacancyByID(id int) (model.Vacancy, error) {
	return s.repo.GetByID(id)
}

func (s *Service) GetVacancyList() ([]model.Vacancy, error) {
	return s.repo.GetList()
}

func (s *Service) DeleteVacancy(id int) error {
	return s.repo.Delete(id)
}

package handler

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	httpSwagger "github.com/swaggo/http-swagger"
)

func (h *Handler) InitRouter() *chi.Mux {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Get("/swagger/*", httpSwagger.Handler(
		httpSwagger.URL("http://localhost:8080/swagger/doc.json")))
	r.Post("/search", h.Search)
	r.Post("/get", h.GetByID)
	r.Post("/delete", h.Delete)
	r.Get("/list", h.GetAll)
	return r
}

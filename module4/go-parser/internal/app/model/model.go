package model

import "encoding/json"

func UnmarshalVacancy(data []byte) (Vacancy, error) {
	var r Vacancy
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Vacancy) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type SwaggerID struct {
	ID int `json:"id" default:"1"`
}

type SwaggerQuerry struct {
	Query string `json:"query"`
}

type Vacancy struct {
	ID             int `json:"id" db:"id"`
	Identifier     `json:"identifier" bson:"identifier""`
	Title          string `json:"title" db:"description" bson:"description"`
	Description    string `json:"description" db:"data_posted" bson:"data_posted"`
	DatePosted     string `json:"datePosted" db:"valid_through" bson:"valid_through""'`
	ValidThrough   string `json:"validThrough" db:"valid_through" bson:"valid_through"`
	EmploymentType string `json:"employmentType" db:"empoyment_type" bson:"employment_type"'`
}

type Identifier struct {
	Value string `json:"value" db:"vacancy_id" bson:"vacancy_id"`
	Name  string `json:"name" db:"organization" bson:"organization"`
}

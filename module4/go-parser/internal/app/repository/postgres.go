package repository

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/egory4eff-x/go-kata/module4/go-parser/internal/app/model"
	"time"
)

type PostgresDB struct {
	db *sqlx.DB
}

func NewPostgresDB() (*PostgresDB, error) {

	// Параметры подключения к базе данных
	//connStr := "user=postgres password=12345 dbname=postgres host=localhost port=5432 sslmode=disable"
	connStr := "user=postgres password=12345 dbname=postgres host=postgresdb port=5432 sslmode=disable"

	// Открытие соединения с базой данных
	db, err := sqlx.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}
	// Проверка соединения с базой данных
	err = db.Ping()
	if err != nil {
		return nil, err
	}

	// создаем подготовленный SQL-запрос для создания таблицы datas в базе данных PostgreSQL
	// если она еще не существует
	createTableSQL := `
			CREATE TABLE IF NOT EXISTS vacancies (
			id bigserial PRIMARY KEY,
			vacancy_id text,
			organization text,
			title text,
			description text,
			date_posted date,
			valid_through date,
			employment_type text,
			updated_at timestamp default now(),
			UNIQUE(vacancy_id)
		)`
	if err != nil {
		return nil, err
	}
	// Вызываем метод Exec объекта stmt, который выполняет подготовленный SQL-запрос.
	// Если таблица уже существует, то этот запрос не будет выполнен и
	// метод Exec вернет ошибку pq: relation "table" already exists. Если таблица еще не существует,
	// то она будет создана, и метод Exec вернет количество строк, затронутых запросом
	if _, err := db.Exec(createTableSQL); err != nil {
		return nil, err
	}
	return &PostgresDB{db: db}, nil
}

func (p *PostgresDB) AddVacancy(vacancy model.Vacancy) error {
	_, err := p.db.Exec(`INSERT INTO vacancies (vacancy_id, organization, title, description, date_posted, valid_through, employment_type, updated_at)
	VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`,
		vacancy.Identifier.Value, vacancy.Identifier.Name, vacancy.Title, vacancy.Description,
		vacancy.DatePosted, vacancy.ValidThrough, vacancy.EmploymentType, time.Now())
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func (p *PostgresDB) GetByID(id int) (model.Vacancy, error) {
	var vacancy model.Vacancy
	err := p.db.Get(&vacancy, "SELECT id, vacancy_id, organization, date_posted, title, description, valid_through, employment_type FROM vacancies WHERE id = $1", id)
	if err != nil {
		return model.Vacancy{}, err
	}
	return vacancy, nil
}

func (p *PostgresDB) GetList() ([]model.Vacancy, error) {
	var vacancies []model.Vacancy
	err := p.db.Select(&vacancies, `
		SELECT id,vacancy_id, organization, title, description, date_posted, valid_through, employment_type FROM vacancies
	`)
	if err != nil {
		return nil, err
	}
	return vacancies, nil
}

func (p *PostgresDB) Delete(id int) error {
	_, err := p.db.Exec("DELETE FROM vacancies WHERE id=$1", id)
	if err != nil {
		return err
	}
	return nil
}

func (p *PostgresDB) Close() error {
	return p.db.Close()
}

package repository

import (
	"errors"
	"gitlab.com/egory4eff-x/go-kata/module4/go-parser/internal/app/model"
	"sync"
)

type RepoDB struct {
	data  map[string]*model.Vacancy
	count int
	sync.RWMutex
}

func NewRepoDB() (*RepoDB, error) {
	return &RepoDB{
		data:  make(map[string]*model.Vacancy),
		count: 1,
	}, nil
}

func (r *RepoDB) AddVacancy(vacancy model.Vacancy) error {
	r.Lock()
	defer r.Unlock()

	identifier := vacancy.Identifier.Value
	if _, ok := r.data[identifier]; !ok {
		r.data[identifier] = &vacancy
		r.data[identifier].ID = r.count
		r.count++
		return nil
	}
	return errors.New("vacancy with ID aleady exists")
}

func (r *RepoDB) GetByID(id int) (model.Vacancy, error) {
	r.RLock()
	defer r.RUnlock()
	for _, value := range r.data {
		if value.ID == id {
			return *value, nil
		}
	}
	return model.Vacancy{}, errors.New("NotFound")
}

func (r *RepoDB) GetList() ([]model.Vacancy, error) {
	r.RLock()
	defer r.RUnlock()
	var vacancies []model.Vacancy
	for _, value := range r.data {
		vacancies = append(vacancies, *value)
	}
	return vacancies, nil
}

func (r *RepoDB) Delete(id int) error {
	r.Lock()
	defer r.Unlock()

	for _, vacancy := range r.data {
		if vacancy.ID == id {
			delete(r.data, vacancy.Identifier.Value)
			return nil
		}
	}
	return errors.New("NotFound")
}

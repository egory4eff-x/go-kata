package repository

import (
	"context"
	"gitlab.com/egory4eff-x/go-kata/module4/go-parser/internal/app/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"sync"
)

type MongoDB struct {
	client     *mongo.Client
	collection *mongo.Collection
	sync.Mutex
}

func NewMongoDB() (*MongoDB, error) {
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://mongodb:27017"))
	if err != nil {
		return nil, err
	}

	collection := client.Database("mongodb").Collection("vacancies")

	return &MongoDB{
		client:     client,
		collection: collection,
	}, nil
}

func (m *MongoDB) AddVacancy(vacancy model.Vacancy) error {
	m.Lock()

	defer m.Unlock()

	counter, err := m.collection.CountDocuments(context.TODO(), bson.M{})
	if err != nil {
		return err
	}

	vacancy.ID = int(counter) + 1

	_, err = m.collection.InsertOne(context.TODO(), vacancy)
	if err != nil {
		return err
	}
	return nil
}

func (m *MongoDB) GetList() ([]model.Vacancy, error) {
	var vacancies []model.Vacancy

	curl, err := m.collection.Find(context.TODO(), bson.D{{}}, options.Find())
	if err != nil {
		return nil, err
	}

	defer curl.Close(context.TODO())

	for curl.Next(context.TODO()) {
		var elem model.Vacancy
		err := curl.Decode(&elem)
		if err != nil {
			return nil, err
		}
		vacancies = append(vacancies, elem)
	}
	return vacancies, nil
}

func (m *MongoDB) Delete(id int) error {
	_, err := m.collection.DeleteOne(context.TODO(), bson.M{"id": id})
	if err != nil {
		return err
	}
	return nil
}

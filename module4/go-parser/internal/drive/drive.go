package drive

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/firefox"
)

const maxTries = 5

func RunFirefoxDriver() (selenium.WebDriver, error) {
	caps := selenium.Capabilities{
		"browserName": "firefox",
	}

	ffCaps := firefox.Capabilities{}
	caps.AddFirefox(ffCaps)

	var wd selenium.WebDriver

	urlPrefix := "http://selenium:4444/wd/hub"

	for i := 1; i <= maxTries; i++ {
		var err error
		wd, err = selenium.NewRemote(caps, urlPrefix)
		if err != nil {
			log.Fatal().Msg(err.Error())
			continue
		}
		return wd, nil
	}
	return nil, fmt.Errorf("failed to create and configure web driver")
}

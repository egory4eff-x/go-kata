package main

import (
	"context"
	"github.com/rs/zerolog/log"
	"gitlab.com/egory4eff-x/go-kata/module4/go-parser/internal/app/handler"
	"gitlab.com/egory4eff-x/go-kata/module4/go-parser/internal/app/repository"
	"gitlab.com/egory4eff-x/go-kata/module4/go-parser/internal/app/service"
	"net/http"
	"os"
	"os/signal"
	"time"
)

//@title Selenium Parser API
//@version 1.0
//@description API service for habr vacancies
//@host localhost:8080

func main() {

	/*
		//загружаем переменные окружения из файла .env в корневой директории проекта
		err:= godotenv.Load()
		if err != nil{
			log.Fatal().Msg("Error loading .env file")
		}
	*/

	dbType := os.Getenv("DBDRIVE")
	var repo service.MyRepository
	var err error
	switch dbType {
	case "postgres":
		repo, err = repository.NewRepoDB()
		if err != nil {
			log.Fatal().Err(err)
		}
	default:
		log.Fatal().Msg("No database type")
	}

	hand := handler.NewHandler(repo)
	port := ":8080"
	server := http.Server{
		Addr:           port,
		Handler:        hand.InitRouter(),
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	go func() {
		log.Info().Msgf("service start at port %v", port)
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.Fatal().Msg("service stop")
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Info().Msg("broke service")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		log.Fatal().Msgf("service fail, %v", err)
	}

	defer func(server *http.Server) {
		err := server.Close()
		if err != nil {

		}
	}(&server)

	log.Info().Msg("service exit")
}
